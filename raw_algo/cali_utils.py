import numpy as np
import pandas as pd
from scipy import signal
from numpy import fft

try:
    import matplotlib.pyplot as pl
except ModuleNotFoundError:
    class FakePlot():
        def figure(self, *args): pass
        def xlabel(self, *args): pass
        def ylabel(self, *args): pass
        def show(self, *args): pass
                
    pl = FakePlot()  

#
# fuction to plot node variables
# input data in the node_name and the data frame from where
# de acc, gyro and mag variables are read
#

global a_s, m_s, g_s
a_s = 0.000244
m_s = 0.00014
g_s = 0.07

def plot_var(node_name, node, label):
    [n_samples, n_col] = node.shape
    samples = np.arange(n_samples)
    pl.figure(node_name + ' ' + label)
    pl.suptitle(node_name + ' ' + label, size=24)
    # acc
    ax1 = pl.subplot(3,3,1)
    pl.plot(samples, node.a_x * a_s, 'r')
    pl.ylabel('Acceleration \n [g]', multialignment='center')
    pl.gca().set_title('X')
    pl.grid()
    pl.subplot(3,3,2, sharex=ax1)
    pl.plot(samples, node.a_y * a_s, 'g')
    pl.gca().set_title('Y')
    pl.grid()
    pl.subplot(3,3,3, sharex=ax1)
    pl.plot(samples, node.a_z * a_s, 'b')
    pl.gca().set_title('Z')
    pl.grid()
    # giro
    pl.subplot(3,3,4, sharex=ax1)
    pl.plot(samples, node.g_x * g_s, 'r')
    pl.ylabel('Angular vel \n [deg/seg]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,5, sharex=ax1)
    pl.plot(samples, node.g_y * g_s, 'g')
    pl.grid()
    pl.subplot(3,3,6, sharex=ax1)
    pl.plot(samples, node.g_z * g_s, 'b')
    pl.grid()
    # mag
    pl.subplot(3,3,7, sharex=ax1)
    pl.plot(samples, node.m_x * m_s, 'r')
    pl.ylabel('Mag field \n [Gauss]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,8, sharex=ax1)
    pl.plot(samples, node.m_y * m_s, 'g')
    pl.grid()
    pl.subplot(3,3,9, sharex=ax1)
    pl.plot(samples, node.m_z * m_s, 'b')
    pl.grid()


# function to filter 1 node, all variables
# gyro, acc and mag

def node_filter(node, fc=5, order=3):
    # geting sampling freq
    [n_samples, n_col] = node.shape
    time = node['time(msec)'].to_numpy()
    dt_tot = time[1:n_samples] - time[0:n_samples - 1]
    dt = np.mean(dt_tot)
    fs = 1 / dt * 1000
    # filter creation (low pass butterworth)
    b, a = signal.butter(order, 2 * np.pi * fc, 'lowpass', analog=True)
    z, po = signal.bilinear(b, a, fs)
    # filtering signal:
    p = node.loc[:,['g_x', 'g_y', 'g_z','a_x', 'a_y', 'a_z','m_x', 'm_y', 'm_z']]
    pf = signal.filtfilt(z, po, p, axis=0)
    node2 = node.copy() # linea agregada
    node2.loc[node.index, ('g_x', 'g_y', 'g_z','a_x', 'a_y', 'a_z','m_x', 'm_y', 'm_z')]=pf.astype('int64') # linea modificada
    return node2 # linea modificada
