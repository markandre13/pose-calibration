#!/usr/bin/env python
# coding: utf-8

import pandas as pd

takes="stella_take01 stella_take06 stella_take08 stella_take09 stella_take10 stella_take11 stella_take12 stella_take13 stella_take14 stella_takeB1 stella_takeB2 stella_takeB3 stella_takeB4"
takes = takes.split(" ")

def convert_to_MATLAB(take):
    INPUT = 'capture_df_calibrated_{}.csv'.format(take)
    OUTPUT = 'MATLAB_matrix_{}_lowerbody.csv'.format(take)

    input_df = pd.read_csv(INPUT)
    input_df.drop(input_df.columns[0], axis=1, inplace=True)
    input_df


    # In[8]:


    cols = input_df.columns.to_list()
    keep_cols = [key for key in cols if len(key) == 3]
    keep_cols

    node_list = input_df['node_label'].unique()
    keep_nodes = [node for node in node_list if "foot" in node]
    keep_nodes += [node for node in node_list if "leg" in node]
    keep_nodes += ['base', 'dorsal']
    keep_nodes


    # In[12]:


    df_list = {}

    for node in keep_nodes:
        node_df = input_df[input_df['node_label'] == node] 
        
        #check there's a unique timestamp per row
        df_size = node_df.size
        df_unique_size = node_df.size
        assert df_size == df_unique_size
        
        node_df = node_df[['time(msec)'] + keep_cols ]
        
        #set timestamp as index
        node_df.set_index('time(msec)', inplace=True)
        
        new_columns = {col:"{}_{}".format(node,col) for col in keep_cols}
        
        node_df.rename(columns = new_columns, inplace = True)
        
        df_list[node] = node_df
        

    result_df = pd.concat(df_list.values(), axis=1, join="inner")
    result_df.reset_index(inplace=True)

    result_df.index = range(1, len(result_df) + 1)
    


    # In[14]:


    result_df.to_csv(OUTPUT, index_label='sample')
    print("written file", OUTPUT)
    return result_df

result_df = None
for take in takes:
    result_df = convert_to_MATLAB(take)


print("======== COLUMNS ========")
print("{:4d}: {}".format(0,"sample"))
for i, col in enumerate(result_df.columns.to_list()):
    print("{:4d}: {}".format(i+1,col))

