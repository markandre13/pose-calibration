import numpy as np
import pandas as pd
import mathutils
from .cali_utils import plot_var, node_filter
import matplotlib.pyplot as pl
from math import pi



PLOT_CALI_FILTERED = True # si se desea plotear variables calibrated and filtered
PLOT_IMP = False # si se desea plotear los analisis
OUT_CSV_SUFIX = "file_rewrite_take-gabri_04"

def calibrate_quat(node, node_analysis, vert_rot, func_rot, vert_i, vert_s):

    # Last rotation to rotate each k-ceptor to Y axis (lateral)
    # this is the pre_quat
    print('-' * 84)
    print('calibracion unity quaternion nodo ' + node_analysis)
    print('-' * 84)

    # reading quat in steady state data from df
    node_a = node.iloc[vert_i:vert_s]
    mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z])
    mat_quat = mat_quat.T
    # apply previous rotations to quats
    for i in range(len(mat_quat)):
        quat_i = mathutils.Quaternion((mat_quat[i, :]))
        new_quat = quat_i @ vert_rot.inverted() @ func_rot.inverted()
        mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]
    # obtain mean
    quat_mean = np.mean(mat_quat, axis=0)
    print('quat vector mean')
    print(quat_mean)
    quat_mean = mathutils.Quaternion((quat_mean))

    # generate reference lateral vector
    lateral = mathutils.Vector((0, 1, 0))  # Y axis in node coordinates
    print('Lateral vector in pyhton system')
    print(lateral)
    # rotate LATERAL with quat_mean
    lateral.rotate(quat_mean)
    print('Lateral vector in world system')
    print(lateral)
    # get GAMA angle to make correction
    gama = np.arctan2(lateral.y, lateral.x)
    print('gama [deg] to rotate the k-ceptor')
    print(-gama *  180 / np.pi)
    # generate rotation
    quat_rot = mathutils.Quaternion((0,0,1), - gama)

    print('pre_quat to align to world')
    print(quat_rot)
    return quat_rot

def apply_rot_2mat(mat, rot):
    for i in range(mat.shape[0]):
        mat_i = mathutils.Vector((mat[i,:]))
        mat_i.rotate(rot)
        mat[i,:] = [mat_i.x, mat_i.y, mat_i.z]
    return mat

class Cali_Mag:
    def __init__(self, df, g_s, a_s, m_s, vert_i, vert_s, fc, output_name) -> None:
        self.capture = df
        n_list = self.capture['node_label'].unique()
        order = np.where(n_list == 'r-upperarm')
        self.node_list = np.roll(n_list, -order[0])
        self.g_s = g_s
        self.a_s = a_s
        self.m_s = m_s
        self.vert_i = vert_i
        self.vert_s = vert_s
        self.fc = fc
        self.output_name = output_name
        self.df_cali_a = pd.DataFrame(index=self.node_list, columns=['w', 'x', 'y', 'z'], dtype=float)
        self.df_cali_a.index.name = 'node_label'
        self.df_cali_b = pd.DataFrame(index=self.node_list, columns=['w', 'x', 'y', 'z'],dtype=float)
        self.df_cali_b.index.name = 'node_label'
        self.df_cali_c = pd.DataFrame(index=self.node_list, columns=['w', 'x', 'y', 'z'],dtype=float)
        self.df_cali_c.index.name = 'node_label'
        self.df_cali_d = pd.DataFrame(index=self.node_list, columns=['w', 'x', 'y', 'z'], dtype=float)
        self.df_cali_d.index.name = 'node_label'
        self.filter()

    def run(self):
        self.jcs_reference()
        self.vert_calib()
        self.mag_mean()
        self.rotations_calib()
        self.write_rotations_files()
        self.apply_calibration()
        self.write_calibrated_data()
        self.plot_nodes()
        self.print_results()

    def vert_calib(self):
        for name in self.node_list:
            print('--' * 84)
            print('NODE UNDER CALIBRATION ' + name)
            print('--' * 84)
            #
            # VERTICAL CALIBRATION
            #
            print('-' * 84)
            print('vertical calibration node ' + name)
            print('-' * 84)
            # reading steady state data from df
            node_a = self.capture[self.capture['node_label'] == name].iloc[self.vert_i:self.vert_s]
            mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z] )* self.a_s
            # get mean
            acc_mean = np.mean(mat_acc, axis=1)
            print('acc vector mean')
            print(acc_mean)
            vec1 = mathutils.Vector((acc_mean))
            # generate reference down vector
            down = mathutils.Vector((0, 0, -1))  # gravity value is negative
            # get rotation
            vert_rot = vec1.rotation_difference(down)
            # rotate vector
            vec1.rotate(vert_rot)
            print('vert_rot - math utils')
            print(vert_rot)
            print('acc mean vector vert rotated')
            print(vec1)
            self.df_cali_a.loc[name, ['w', 'x', 'y', 'z']] = [vert_rot.w, vert_rot.x, vert_rot.y, vert_rot.z]

    def filter(self):
        for name in self.node_list:
            node = self.capture[self.capture['node_label'] == name]
            node = node_filter(node, self.fc, order=3)
            self.capture.loc[self.capture['node_label'] == name, node.columns] = node

    def mag_mean(self):
        # prepare DF to save mean of mag data
        mag_all_mean = pd.DataFrame(index =self.node_list, columns =['x', 'y', 'z'])
        print('--' * 84)
        print('GETTING MAGNETIC MEAN FROM K-CEPTORS USIGN VERT_ROT')
        print('--' * 84)
        for node_analysis in self.node_list:
            print('-' * 84)
            print('magnetic mean ' + node_analysis)
            print('-' * 84)
            # reading steady state data from df
            df = self.capture[self.capture['node_label'] == node_analysis]  # sending only the node to analyze
            node_a = df.iloc[self.vert_i:self.vert_s]
            mat_acc = np.array([node_a.m_x, node_a.m_y, node_a.m_z] )* self.m_s
            # get mean
            mag_mean = np.mean(mat_acc, axis=1)
            print('mag vector mean')
            print(mag_mean)
            # getting vert_rot from df
            vert_rot = self.df_cali_a.loc[node_analysis]
            # converting to quat
            vert_rot = mathutils.Quaternion((vert_rot))
            # addind 1 dim to mag data
            mag_mean = np.expand_dims(mag_mean, axis=0)
            # apply vert rotation
            mag_mean = apply_rot_2mat(mag_mean, vert_rot)
            print('acc vector mean vert rotated')
            print(mag_mean)
            mag_all_mean.loc[node_analysis, ['x', 'y', 'z']] = mag_mean.squeeze()

        print('df with all mean of magnetometers')
        print('(use only x and y axis)')
        print(mag_all_mean)
        print('overall mean vector to align all k-ceptors')
        print('(use only x and y axis)')
        print(mag_all_mean.mean().to_numpy())

        print('--' * 84)
        print('NOW WE FIND THE ROTATION TO APPLY TO EACH K-CEPTOR')
        print('--' * 84)
        # generate reference vector where all k-ceptor should point at
        ref_nodes = ['dorsal', 'base']
        references = mag_all_mean.loc[ref_nodes]
        print("mag references: {}\n".format(ref_nodes),references.to_numpy())
        first_ref = mathutils.Vector((references.iloc[0]["x"], references.iloc[0]["y"], 0))
        # for i in range(references.shape[0]):
        #     this_ref = mathutils.Vector((references.iloc[i]["x"], references.iloc[i]["y"], 0))
        #     rot_diff = first_ref.rotation_difference(this_ref)
        #     print("ROT DIFF", references.iloc[i].index, rot_diff, rot_diff.angle, rot_diff.angle * 360 / (pi*2))
        #     if abs(rot_diff.angle) > pi/2: #if there's more than 90 degs
        #         this_ref.rotate(mathutils.Euler((0,0,pi))) # rotate the reference 180deg
        #         this_ref.z = references.iloc[i]["z"]
        #         references.iloc[i] = tuple(this_ref)

        mag_vector = references.to_numpy().mean(axis=0)
        ref_mag = mathutils.Vector((mag_vector[0],mag_vector[1],0))  # using mean of all k-ceptors
        
        print('reference mean vector')
        print(ref_mag)
        for node_analysis in self.node_list:
            # get mean of k-ceptor from df
            mag_mean = mag_all_mean.loc[node_analysis].to_numpy()
            # generate mag vector using X and Y only
            vec2 = mathutils.Vector((mag_mean[0], mag_mean[1], 0))
            print('mean vetor of k-ceptor')
            print(vec2)
            # get rotation
            func_rot = vec2.rotation_difference(ref_mag)
            # adding a correction to rotate -90deg in z axis
            correc_rot = mathutils.Quaternion((0.0, 0.0, 1.0), -np.pi) # this correction is needed... i dont know why :/
                                                                           # it might be the axis of mag data (X and Y)
                                                                           # and how the mathtuils vectos are defined
                                                                           # i get lost in this part of the rotations...
            if ref_mag.y < 0 :
                func_rot = func_rot @ correc_rot
            print('functional rotation to apply', func_rot.angle * 360 / (2*pi))
            print(func_rot)
            rot_vec = mathutils.Vector(mag_mean)
            rot_vec.rotate(mathutils.Quaternion(func_rot))
    
            self.df_cali_b.loc[node_analysis, ['w', 'x', 'y', 'z']] = [func_rot.w, func_rot.x, func_rot.y, func_rot.z]
        
    
    def jcs_reference(self):
        self.trunk_rot = mathutils.Euler((-np.pi/2 + np.pi, 0, 0)).to_quaternion().inverted()
        self.foot_rot = mathutils.Euler((0, -np.pi/2, -np.pi/2)).to_quaternion().inverted()
        #limbs_rot = mathutils.Euler((-np.pi/2, 0, 0), 'ZYX') #<--- euler.order = 'nnn' NOT DEFINED IN MATHUTILS-PYTHON
        # see https://gitlab.com/ideasman42/blender-mathutils/-/issues/10
        # this is solved by using 3 single-axis quaternion and combined accordingly
        # single-axis quaternions
        limbs_rot_x = mathutils.Quaternion((1,0,0), -np.pi/2)
        limbs_rot_y = mathutils.Quaternion((1,0,0), 0)
        limbs_rot_z = mathutils.Quaternion((1,0,0), 0)
        self.limbs_rot = (limbs_rot_z @ limbs_rot_y @ limbs_rot_x).inverted()

    def rotations_calib(self):
        for name in self.node_list:
            vert_rot = mathutils.Quaternion((self.df_cali_a.loc[name]))
            func_rot = mathutils.Quaternion((self.df_cali_b.loc[name]))
            node = self.capture[self.capture['node_label'] == name]
            quat_rot = calibrate_quat(node, name, vert_rot, func_rot, self.vert_i, self.vert_s)
            quat_jcs = mathutils.Quaternion()
            if name in ('base', 'dorsal', 'neck'):
                quat_jcs = self.trunk_rot
            elif name not in ('l-foot', 'r-foot'):
                quat_jcs = self.limbs_rot
            elif name in ('l-foot', 'r-foot'):
                quat_jcs = self.foot_rot

            pos_rot = vert_rot.inverted() @ func_rot.inverted() @ quat_jcs.inverted()

            self.df_cali_c.loc[name, ['w', 'x', 'y', 'z']] = [quat_rot.w, quat_rot.x, quat_rot.y, quat_rot.z]
            self.df_cali_d.loc[name, ['w', 'x', 'y', 'z']] = [pos_rot.w, pos_rot.x, pos_rot.y, pos_rot.z]

    def write_rotations_files(self):
        self.df_cali_a.to_csv('data/rotation_vert_{}.csv'.format(self.output_name))
        self.df_cali_b.to_csv('data/rotation_func_{}.csv'.format(self.output_name))
        self.df_cali_c.to_csv('data/rotation_pre_{}.csv'.format(self.output_name))
        self.df_cali_d.to_csv('data/rotation_pos_{}.csv'.format(self.output_name))

    def apply_calibration(self):
        for name in self.node_list:
            node_a = self.capture[self.capture['node_label'] == name]

            mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z]).T
            mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z]).T
            mat_mag = np.array([node_a.m_x, node_a.m_y, node_a.m_z]).T
            mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z]).T

            vert_rot = mathutils.Quaternion((self.df_cali_a.loc[name]))
            func_rot = mathutils.Quaternion((self.df_cali_b.loc[name]))
            quat_rot = mathutils.Quaternion((self.df_cali_c.loc[name]))
            pos_rot = mathutils.Quaternion((self.df_cali_d.loc[name]))

            mat_acc = apply_rot_2mat(mat_acc, vert_rot)
            mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)
            mat_mag = apply_rot_2mat(mat_mag, vert_rot)
            mat_acc = apply_rot_2mat(mat_acc, func_rot)
            mat_gyro = apply_rot_2mat(mat_gyro, func_rot)
            mat_mag = apply_rot_2mat(mat_mag, func_rot)
            quat_jcs = mathutils.Quaternion()
            if name in ("base", "dorsal", "neck"):
                quat_jcs = self.trunk_rot
            elif name not in ("l-foot", "r-foot"):
                quat_jcs = self.limbs_rot
            elif name in ("l-foot", "r-foot"):
                quat_jcs =  self.foot_rot

            mat_acc = apply_rot_2mat(mat_acc, quat_jcs)
            mat_gyro = apply_rot_2mat(mat_gyro, quat_jcs)
            mat_mag = apply_rot_2mat(mat_mag, quat_jcs)

            for i in range(len(mat_quat)):
                quat_i = mathutils.Quaternion((mat_quat[i, :]))
                new_quat = quat_rot @ quat_i @ pos_rot
                mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]

            ref_index = self.capture[self.capture['node_label'] == name]
            self.capture.loc[ref_index.index, ['a_x', 'a_y', 'a_z']] = mat_acc
            self.capture.loc[ref_index.index, ['g_x', 'g_y', 'g_z']] = mat_gyro
            self.capture.loc[ref_index.index, ['m_x', 'm_y', 'm_z']] = mat_mag
            self.capture.loc[ref_index.index, ['q_w', 'q_x', 'q_y', 'q_z']] = mat_quat

    def write_calibrated_data(self):
        self.capture.to_csv('data/capture_df_calibrated_{}.csv'.format(self.output_name))

    def plot_nodes(self):
        if PLOT_CALI_FILTERED and pl.matplotlib.is_interactive():
            label = 'cali + filt'
            for name in self.node_list:
                node = self.capture[self.capture['node_label'] == name]
                plot_var(name, node, label)

    def print_results(self):
        vert_i = 200
        vert_s = 300

        node_list = ['l-hand', 'l-lowerarm', 'l-upperarm', 'l-foot', 'l-lowerleg', 'l-upperleg',
                     'r-hand', 'r-lowerarm', 'r-upperarm', 'r-foot', 'r-lowerleg', 'r-upperleg',
                     'neck', 'dorsal', 'base']
        for node_i in node_list:
            node = self.capture[self.capture['node_label'] == node_i]
            # reading gyro data from functional gyro-validation phase
            node_a = node.iloc[vert_i:vert_s]
            mat_gyro = np.array([node_a.m_x, node_a.m_y, node_a.m_z]) * m_s
            mat_gyro = mat_gyro.T
            if node_i in ['l-lowerarm', 'l-upperarm', 'l-lowerleg', 'l-upperleg','r-lowerarm', 'r-upperarm','r-lowerleg', 'r-upperleg']:
                print(str(node_i) + ' \t\t ' + str(np.mean(mat_gyro, axis=0)))
            else:
                print(str(node_i) + ' \t\t\t ' + str(np.mean(mat_gyro, axis=0)))
        if pl.matplotlib.is_interactive():
            pl.show()

# df = pd.read_csv('../test_data/Tecnocampus_masha_06.csv')
# a = Cali_Mag(df, 0.000244, 0.00014, 0.07, 250, 300, 3, 'masha-take')
# a.run()
