from os import path

to_return = {}

data_dir = "data/stella"
data_dir = path.join(path.dirname(path.realpath(__file__)) , data_dir)

# # Stella take 01
OUT_CSV_SUFIX = "stella_take01"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 117,
    "vert_s" : 300, #402,

    "func_arms_i" : 350, #457,
    "func_arms_s" : 500, #680,

    "func_trunk_i" : 733,
    "func_trunk_s" : 1200, #1068,

    "func_legs_i" : 1200, #1110,
    "func_legs_s" : 1550 #1419
}

# # Stella take 06
OUT_CSV_SUFIX = "stella_take06"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 20,
    "vert_s" : 233,

    "func_arms_i" : 225,
    "func_arms_s" : 450,

    "func_trunk_i" : 454,
    "func_trunk_s" : 842,

    "func_legs_i" : 877,
    "func_legs_s" : 1149
}
# # Stella take 08
OUT_CSV_SUFIX = "stella_take08"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 110,
    "vert_s" : 205,

    "func_arms_i" : 330,
    "func_arms_s" : 545,

    "func_trunk_i" : 525,
    "func_trunk_s" : 844,

    "func_legs_i" : 879,
    "func_legs_s" : 1100
}
# # Stella take 09
OUT_CSV_SUFIX = "stella_take09"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 50,
    "vert_s" : 196,

    "func_arms_i" : 202,
    "func_arms_s" : 415,

    "func_trunk_i" : 435,
    "func_trunk_s" : 727,

    "func_legs_i" : 745,
    "func_legs_s" : 1096
}
# # Stella take 10
OUT_CSV_SUFIX = "stella_take10"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 126,
    "vert_s" : 265,

    "func_arms_i" : 266,
    "func_arms_s" : 504,

    "func_trunk_i" : 534,
    "func_trunk_s" : 859,

    "func_legs_i" : 904,
    "func_legs_s" : 1218
}
# # Stella take 11
OUT_CSV_SUFIX = "stella_take11"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 163,
    "vert_s" : 500,

    "func_arms_i" : 600,
    "func_arms_s" : 785,

    "func_trunk_i" : 798,
    "func_trunk_s" : 1123,

    "func_legs_i" : 1177,
    "func_legs_s" : 1650
}

# # Stella take 12
OUT_CSV_SUFIX = "stella_take12"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 40,
    "vert_s" : 160,

    "func_arms_i" : 200,
    "func_arms_s" : 380,

    "func_trunk_i" : 380,
    "func_trunk_s" : 662,

    "func_legs_i" : 718,
    "func_legs_s" : 1241
}

# # Stella take 13
OUT_CSV_SUFIX = "stella_take13"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 100,
    "vert_s" : 200,

    "func_arms_i" : 271, #230,
    "func_arms_s" : 416, #472,

    "func_trunk_i" : 491,
    "func_trunk_s" : 813,

    "func_legs_i" : 869,
    "func_legs_s" : 1200
}

# # Stella take 14
OUT_CSV_SUFIX = "stella_take14"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 100,
    "vert_s" : 245,

    "func_arms_i" : 243,
    "func_arms_s" : 433,

    "func_trunk_i" : 465,
    "func_trunk_s" : 687,

    "func_legs_i" : 723,
    "func_legs_s" : 1035
}

# # Stella take B1
OUT_CSV_SUFIX = "stella_takeB1"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 100,
    "vert_s" : 250,

    "func_arms_i" : 270,
    "func_arms_s" : 474,

    "func_trunk_i" : 480,
    "func_trunk_s" : 714,

    "func_legs_i" : 800,
    "func_legs_s" : 1100
}

# # Stella take B2
OUT_CSV_SUFIX = "stella_takeB2"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 174,
    "vert_s" : 323,

    "func_arms_i" : 354,
    "func_arms_s" : 547,

    "func_trunk_i" : 600,
    "func_trunk_s" : 900,

    "func_legs_i" : 911,
    "func_legs_s" : 1243
}

# # Stella take B3
OUT_CSV_SUFIX = "stella_takeB3"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 117,
    "vert_s" : 402,

    "func_arms_i" : 457,
    "func_arms_s" : 680,

    "func_trunk_i" : 733,
    "func_trunk_s" : 1068,

    "func_legs_i" : 1110,
    "func_legs_s" : 1419
}

# # Stella take B4
OUT_CSV_SUFIX = "stella_takeB4"
to_return[OUT_CSV_SUFIX] = {
    "NAME" : path.join(data_dir, '2021_11_30_dump_{}.csv'.format(OUT_CSV_SUFIX)),

    "OUT_CSV_SUFIX" : OUT_CSV_SUFIX,
    "vert_i" : 200,
    "vert_s" : 300,

    "func_arms_i" : 373,
    "func_arms_s" : 626,

    "func_trunk_i" : 640,
    "func_trunk_s" : 930,

    "func_legs_i" : 989,
    "func_legs_s" : 1300
} 


def get(key):
    if key not in to_return.keys():
        raise KeyError("The key {} is not available in the 'stella' capture series".format(key))

    r = to_return[key]
    
    return (r["NAME"], r["OUT_CSV_SUFIX"], r["vert_i"], r["vert_s"], r["func_arms_i"], r["func_arms_s"], 
r["func_trunk_i"], r["func_trunk_s"], r["func_legs_i"], r["func_legs_s"])