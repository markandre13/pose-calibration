# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import numpy as np
import pandas as pd
import mathutils
# from sklearn.decomposition import PCA
from .cali_utils import plot_var, node_filter
from scipy.signal import find_peaks

try:
    import matplotlib.pyplot as pl
except ModuleNotFoundError:
    class FakePlot():
        def figure(self, *args): pass
        def xlabel(self, *args): pass
        def ylabel(self, *args): pass
        def show(self, *args): pass
                
    pl = FakePlot()    
    
def _PCA_np(X):
        # center the data
        X_s = (X - X.mean(axis=0)) #/ (X.std(axis=0))
        # Compute SVD (its better than  the EINGENVECTOR method
        #  (no covariance problem))
        u, s, vh = np.linalg.svd(X_s.T, full_matrices=True)
        # enforce signs convention
        # (largest element in each eigenvector must be positive)
        max_index = np.argmax(np.abs(u), axis=0)
        for col in range(np.size(u, 1)):
            sign = np.sign(u[max_index[col], col])
            u[:, col] = u[:, col] * sign

        # Calculating the 1st component angle (w.r.t. WORLD X-Y)
        frst_comp_ang = np.arctan2(u[1, 0], u[0, 0])
        return u, s, frst_comp_ang   
    
PCA=_PCA_np   

#
# Global variables definition
#

a_s = 0.000244
m_s = 0.00014
g_s = 0.07

#
# DATA LOADING SECTION
#
# loading *.csv file of calibration capture

from .capture_ranges import *

from . import stella_ranges
from sys import argv

take = 'stella_takeB3'


# if len(argv) > 1:
#     take = argv[1] #pass the take name as a command line argunment

(NAME, OUT_CSV_SUFIX, vert_i, vert_s, 
func_arms_i, func_arms_s,
func_trunk_i, func_trunk_s, 
func_legs_i, func_legs_s) = stella_ranges.get(take)

capture = pd.read_csv(NAME) # <----------- FILE TO LOAD
#capture = pd.read_csv('data/2021_09_23_CSV_dump_capture2_take-08.csv') # <----------- FILE TO LOAD
#capture = pd.read_csv('data/2021_09_23_CSV_dump_capture2_take-02_Kalman_applied_m.csv')
#
# OUTPUT CONTROLS SECTION
#
PLOT_CALI_FILTERED = False # si se desea plotear variables calibrated and filtered
PLOT_IMP = False # si se desea plotear los analisis

###############################################
#
# CHOOSE THE OUTPUT REF SYSTEM FOR THE VECTORS
#
###############################################

# === JCS coordinate system ====
#The vector is represented in terms of the reference frame of its bone
#i.e: the gravity points in the direction the bone is pointing in the JCS (in rest pose)
# OUT_DF_VEC_COORDS = "JCS" 

# === WORLD coordinate system ===
# the vector is represented in the global reference frame
# i.e: the gravity always points in -Z
OUT_DF_VEC_COORDS = "WORLD" 

# === WORLD INITIAL coordinate system ===
# the vector is represented in terms of the global reference frame
# i.e: the gravity points in -Z during rest pose
# OUT_DF_VEC_COORDS = "INITIAL_WORLD"



# OUT_CSV_SUFIX = "take-08"
NODES_TO_PLOT = ( 'neck', 'r-upperarm', 'r-upperleg')
NODES_TO_PLOT = ( 'r-upperarm')

LEGS_ROTATED_HACK = False
# if len(argv) > 2: # pass the LEGS_ROTATED_HACK as second cli argument
#     if argv[2] in ("true", "True", "1"):
#         LEGS_ROTATED_HACK = True
#
# cut frequency for filter and initializacion of number_samples_step (n_s_p)
#
fc = 3
n_s_p = 0 #<------------------ number of sample per step (walking phase)

# globally control the output
PRINT_DEBUG = False
def debug(*args):
    if PRINT_DEBUG:
        print(*args)


# funcion para hallar la rotacion vertical de calibracion
def vert_calib(df, node_analysis, vert_i, vert_s, graf):
    # df -> data frame with node data (acc, gyro and mag)
    # node_analysis -> node to analyse
    # vert_i -> inferior index for vertical calibration
    # vert_s -> superior index for vertical calibration
    # graf -> if graf == 1, plots analysis

    debug('--' * 84)
    debug('NODE UNDER CALIBRATION ' + node_analysis)
    debug('--' * 84)
    #
    # VERTICAL CALIBRATION
    #
    debug('-' * 84)
    debug('vertical calibration node ' + node_analysis)
    debug('-' * 84)
    # reading steady state data from df
    node_a = df.iloc[vert_i:vert_s]
    mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z] )* a_s
    # get mean
    acc_mean = np.mean(mat_acc, axis=1)
    debug('acc vector mean')
    debug(acc_mean)
    vec1 = mathutils.Vector((acc_mean))
    # generate reference down vector
    down = mathutils.Vector((0, 0, -1))  # gravity value is negative
    # get rotation
    vert_rot = vec1.rotation_difference(down)
    # rotate vector
    vec1.rotate(vert_rot)
    debug('vert_rot - math utils')
    debug(vert_rot)
    debug('acc mean vector vert rotated')
    debug(vec1)

    return vert_rot

partials = {}

def add_to_partials(process, label, result):
    global partials
    if process not in partials.keys():
        partials[process] = {}
        
    partials[process][label] = result

# funcion para hallar la rotacion funcional de calibracion
def func_calib(df, node_analysis, vert_rot, func_i, func_s, graf):
    # df -> data frame with node data (acc, gyro and mag)
    # node_analysis -> node to analyse
    # ver_rot -> mathutils quaternion of vertical rotation
    # func_i -> inferior index for vertical calibration
    # func_s -> superior index for vertical calibration
    # graf -> if graf == 1, plots analysis
    #
    # FUNCTIONAL CALIBRATION (GYRO)
    #
    debug('-' * 84)
    debug('functional calibration node ' + node_analysis)
    debug('-' * 84)
    # reading gyro data from functional phase
    node_a = df.iloc[func_i:func_s]
    mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z] ) * g_s
    mat_gyro = mat_gyro.T
    # apply vert_rot rotation
    mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)


    # PCA step
    # arrange data to feed PCA
    datos = np.array([mat_gyro[:, 0] , mat_gyro[:, 1]])
    datos = datos.T
    # run PCA
    eigen_vectors, sing_values, angle_new = PCA(datos)
    
    debug('PCA singular values')
    debug(sing_values)
    sv = sing_values
    debug('PCA components from ' + node_analysis)
    debug(eigen_vectors)

    # here comes the angle to rotate the z axis
    angle = angle_new
    angle_deg = angle* 360 / (2 * np.pi)
    debug('1st component angle:')
    debug(angle_deg)
    debug('angle to rotate:')
    debug((np.pi / 2 - angle) * 360 / (2 * np.pi))
    # generate rotation
    func_rot = mathutils.Quaternion((0, 0, 1), np.pi/2- angle)
    debug('func_rot')
    debug(func_rot)

    add_to_partials("PCA", node_analysis, func_rot.copy())
    #
    # GYRO verification
    #
    # check correct GYRO sign accordingly calibration procedure
    debug('-' * 84)
    debug('gyro sign check node ' + node_analysis)
    debug('-' * 84)


    if node_analysis not in ['r-upperleg', 'r-lowerleg', 'r-foot', 'l-upperleg', 'l-lowerleg', 'l-foot']:
        func_gyro_i = func_i
        func_gyro_s = (func_s - func_i) // 2 + func_i # half of the func_samples
        # reading gyro data from functional gyro-validation phase
        node_a = df.iloc[func_gyro_i:func_gyro_s]
        mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z]) * g_s
        mat_gyro = mat_gyro.T

        # apply vert rotation
        mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)
        # apply func rotation
        mat_gyro = apply_rot_2mat(mat_gyro, func_rot)

        # plot data of mean analysis
        if graf == 1:
            if node_analysis in NODES_TO_PLOT:
                fig = pl.figure('mean value ' + node_analysis)
                ax = fig.add_subplot(111)
                ax.set_title('mean value  ' + node_analysis, y=0.9)
                ax.plot(mat_gyro[:,0], 'r', mat_gyro[:,1], 'g',mat_gyro[:,2], 'b')
                pl.xlabel('sample')
                pl.ylabel('gyro Y')
                pl.legend(('x','y', 'z'))
                ax.grid()

        # obtain dt for integrate
        [n_samples, n_col] = df.shape
        time = df['time(msec)'].to_numpy()
        dt_tot = time[1:n_samples] - time[0:n_samples - 1]
        dt = np.mean(dt_tot)
        
        # obtain integral
        gyro_int = np.mean(mat_gyro, axis=0)* g_s / mat_gyro.shape[0] * dt
        add_to_partials("gyro_int", node_analysis, gyro_int.copy())
        debug('inital gyro int')
        debug(gyro_int )
        debug('inital gyro int deg')
        debug(gyro_int *180/np.pi)
        
        # get dorsal mag mean for lower limb gyro verification
        # this will be used to check the correct sign of gyro data
        # in lower legs
        if node_analysis == 'r-upperarm':
            debug('---'*16)
            debug('magnetic mean in steady state for lower limbs gyro validation')
            debug('---' * 16)
            global mag_r_upperarm_mean, coor
            # reading mag data from vertical calibration phase
            node_a = df.iloc[vert_i:vert_s]
            mat_mag = np.array([node_a.m_x, node_a.m_y, node_a.m_z]) * m_s
            mat_mag = mat_mag.T
            # apply vert rotation
            mat_mag = apply_rot_2mat(mat_mag, vert_rot)
            # apply func rotation
            mat_mag = apply_rot_2mat(mat_mag, func_rot)
            # obtain mean
            mag_r_upperarm_mean = np.mean(mat_mag, axis=0)
            add_to_partials("mag_mean", node_analysis, mag_r_upperarm_mean.copy())
            debug('r-upperarm mag mean in steady state')
            debug(mag_r_upperarm_mean)
            # get the max component of gravity vector
            coor = np.argmax(np.abs(mag_r_upperarm_mean[0:2])) #<----- por que solo tomo x e y? por que en z es maximo
                                                                # y no me sirve para orientar
            debug('idex of max coordinate: ' + str(coor))
            debug('max coordinate: ' + str(mag_r_upperarm_mean[coor]))

    #  #  #  #  # HERE STARTS GYRO VERIFICATION WITH MAG DATA IN LOWER LIMBS
    if node_analysis in ['r-upperleg', 'r-lowerleg', 'r-foot', 'l-upperleg', 'l-lowerleg', 'l-foot']:
        # reading mag data from vertical calibration phase
        node_a = df.iloc[vert_i:vert_s]
        mat_mag = np.array([node_a.m_x, node_a.m_y, node_a.m_z]) * m_s
        mat_mag = mat_mag.T
        # apply vert rotation
        mat_mag = apply_rot_2mat(mat_mag, vert_rot)
        # apply func rotation
        mat_mag = apply_rot_2mat(mat_mag, func_rot)
        # obtain mean od node
        mat_mag_mean = np.mean(mat_mag, axis=0)
        add_to_partials("mag_mean", node_analysis, mat_mag_mean.copy())

        debug( node_analysis + ' mag mean in steady state')
        debug(mat_mag_mean)
        # find max mag index
        coor2 = np.argmax(np.abs(mat_mag_mean[0:2]))
        debug('idex of max coordinate: ' + str(coor2))
        debug('r-upperarm mag mean in steady state to compare with')
        debug(mag_r_upperarm_mean)
        debug('idex of max coordinate: ' + str(coor))
        # make comparisson
        sign = mat_mag_mean[1] * mag_r_upperarm_mean[1]
        debug('result of comparisson: ' + str(sign))
        gyro_int = np.array([1,1*sign,1])
        add_to_partials("gyro_int", node_analysis, gyro_int.copy())
        debug(gyro_int)

    #  #  #  #  # HERE ENDS GYRO VERIFICATION WITH MAG DATA IN LOWER LIMBS
    
    if LEGS_ROTATED_HACK:
        bones_change_180 = ['neck', 'dorsal', 'base','r-upperleg', 'r-lowerleg', 'r-foot', 'l-upperleg', 'l-lowerleg', 'l-foot']
    else:
        bones_change_180 = ['neck', 'dorsal', 'base']

    if node_analysis not in bones_change_180 :
        if gyro_int[1] > 0:
            func_rot = mathutils.Quaternion((0, 0, 1), np.pi / 2 - angle + np.pi)
            gyro_int = gyro_int * -1
            debug('gyro changed 180 deg')
            print("INVERT compare", node_analysis)
            add_to_partials('inverted', node_analysis, True)
        else:
            debug('gyro NOT changed 180 deg')
            add_to_partials('inverted', node_analysis, False)

    if node_analysis in bones_change_180 :
        if gyro_int[1] < 0:
            func_rot = mathutils.Quaternion((0, 0, 1), np.pi / 2 - angle + np.pi)
            gyro_int = gyro_int * -1
            debug('gyro changed 180 deg')
            print("INVERT compare", node_analysis)
            add_to_partials('inverted', node_analysis, True)
            
        else:
            debug('gyro NOT changed 180 deg')
            add_to_partials('inverted', node_analysis, False)
    debug('final gyro int in deg')
    debug(gyro_int *180/np.pi)
    add_to_partials("final_gyro_int", node_analysis, gyro_int.copy())
    add_to_partials("final_func_rot", node_analysis, func_rot.copy())
    

    # to plot analysis in functional calibration (PCA)
    if graf == 1:
        if node_analysis in NODES_TO_PLOT:
            fig = pl.figure('analysis ' + node_analysis)
            ax = fig.add_subplot(111)
            ax.set_title('giro analysis ' + node_analysis, y=0.9)
            ax.plot(datos[:, 0], datos[:, 1], 'k.')
            ax.plot([0, pca.components_[0][0] * sv[0]], [0, pca.components_[0][1] * sv[0]], 'm')
            ax.plot([0, pca.components_[1][0] * sv[1]], [0, pca.components_[1][1] * sv[1]], 'y')
            ax.text(pca.components_[0][0] * sv[0] / 2, pca.components_[0][1] * sv[0] / 2, '%.2f' % angle_deg, fontdict=None)
            pl.xlabel('x')
            pl.ylabel('y')
            ax.set_aspect('equal')
            ax.grid()

    return func_rot, gyro_int

# funion para hallar la PRE rotacion de los k-ceptor
def quat_calib(df, node_analysis, vert_rot, func_rot, gyro_int, vert_i, vert_s, graf):
    # df -> data frame with node data (acc, gyro and mag)
    # node_analysis -> node to analyse
    # ver_rot -> mathutils quaternion of vertical rotation
    # func_rot -> mathutils quaternion of functional rotation
    # gyro_int -> gyro integral for upper-arms correction
    # vert_i -> inferior index for vertical calibration
    # vert_s -> superior index for vertical calibration
    # graf -> if graf == 1, plots analysis

    # Last rotation to rotate each k-ceptor to Y axis (lateral)
    # this is the pre_quat
    # gyro_int is used for make the upper-arm correction.. is used only in
    # those nodes...
    debug('-' * 84)
    debug('calibracion unity quaternion node ' + node_analysis)
    debug('-' * 84)

    # reading quat in steady state data from df
    node_a = df.iloc[vert_i:vert_s]
    mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z])
    mat_quat = mat_quat.T

    # apply previous rotations to quats
    for i in range(len(mat_quat)):
        quat_i = mathutils.Quaternion((mat_quat[i, :]))
        new_quat = quat_i @ vert_rot.inverted() @ func_rot.inverted()
        mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]

    # obtain mean
    quat_mean = np.mean(mat_quat, axis=0)
    debug('quat vector mean')
    debug(quat_mean)
    quat_mean = mathutils.Quaternion((quat_mean))

    # generate reference lateral vector
    lateral = mathutils.Vector((0, 1, 0))  # Y axis in node coordinates
    debug('Lateral vector in pyhton system')
    debug(lateral)

    # rotate LATERAL with quat_mean
    lateral.rotate(quat_mean)
    debug('Lateral vector in world system')
    debug(lateral)

    # get GAMA angle to make correction
    gama = np.arctan2(lateral.y, lateral.x)
    debug('gama [deg] to rotate the k-ceptor')
    debug(-gama *  180 / np.pi)

    # generate rotation
    quat_rot = mathutils.Quaternion((0,0,1), - gama)
    debug('pre_quat to align to world')
    debug(quat_rot)
    
    add_to_partials("heading_rot", node_analysis, quat_rot.copy())
    
    return quat_rot


def apply_rot_2mat(mat, rot):
    for i in range(mat.shape[0]):
        mat_i = mathutils.Vector((mat[i,:]))
        mat_i.rotate(rot)
        mat[i,:] = [mat_i.x, mat_i.y, mat_i.z]
    return mat

def apply_quatmat_2mat(mat, quatmat, timestamps):
    assert mat.shape[0] == quatmat.shape[0], "the lengths of the vec and rotation matrix should be equal"
    assert mat.shape[1] == 3, "the first matrix should be Nx3, representing a list of vectors"
    assert quatmat.shape[1] == 4, "the second matrix should be Nx4, representing a list of quaterninons "
    
    for i in range(mat.shape[0]):
        mat_i = mathutils.Vector((mat[i,:]))
        vec = mat_i.copy()
        quat_i = mathutils.Quaternion((quatmat[i,:]))
        mat_i.rotate(quat_i)
        mat[i,:] = [mat_i.x, mat_i.y, mat_i.z]
        
        # print("at time ({}): \nvec: {}\n quat:{}\n result:{}".format(timestamps[i], vec, quat_i, mat_i))
        # input()
    return mat

#################################################################################################
# HERE STARTS THE CODE TO CALIBRATE
#################################################################################################
if __name__ == "__main__":

    # taking list of nodes to calibrate
    node_list = capture['node_label'].unique()


    debug(node_list)
    orden = np.where(node_list == 'r-upperarm')
    debug(orden)
    node_list = np.roll(node_list, -orden[0])
    debug(node_list)



    # prepare df to save output (calibrations quaternions)
    df_cali_a = pd.DataFrame(index =node_list, columns =['w', 'x', 'y', 'z'])
    df_cali_a.index.name = 'node_label'
    df_cali_b = pd.DataFrame(index =node_list, columns =['w', 'x', 'y', 'z'])
    df_cali_b.index.name = 'node_label'
    df_cali_c = pd.DataFrame(index =node_list, columns =['w', 'x', 'y', 'z'])
    df_cali_c.index.name = 'node_label'
    df_cali_d = pd.DataFrame(index =node_list, columns =['w', 'x', 'y', 'z'])
    df_cali_d.index.name = 'node_label'

    # ranges of data to do calibration are load in the first part of the code

    ############################################################################
    #
    # df filtering
    #
    ###########################################################################
    for nombre in node_list:

        node = capture[capture['node_label'] == nombre]  # sending only the node to analyze
        node = node_filter(node, fc, order=3)
        capture.loc[capture['node_label'] == nombre, node.columns] = node


    ############################################################################
    #
    # JCS reference system definition
    #
    ###########################################################################

    trunk_rot = mathutils.Euler((-np.pi/2+np.pi, 0,0)).to_quaternion().inverted()

    #limbs_rot = mathutils.Euler((-np.pi/2, 0, 0), 'ZYX') #<--- euler.order = 'nnn' NOT DEFINED IN MATHUTILS-PYTHON
    # see https://gitlab.com/ideasman42/blender-mathutils/-/issues/10
    # this is solved by using 3 single-axis quaternion and combined accordingly
    # single-axis quaternions
    limbs_rot_x = mathutils.Quaternion((1,0,0),-np.pi/2)
    limbs_rot_y = mathutils.Quaternion((1,0,0),0)
    limbs_rot_z = mathutils.Quaternion((1,0,0),0)

    # quaternion composition
    limbs_rot = limbs_rot_z @ limbs_rot_y @ limbs_rot_x
    limbs_rot = limbs_rot.inverted()
    foot_rot = mathutils.Euler((-np.pi/2,-np.pi/2,0)).to_quaternion().inverted()



    ############################################################################
    #
    # WCS reference system definition (to rotate from JCS to WOLRD) <------------------------------------------------------ los nuevos quaterniones para llevar JCS a world
    #
    ###########################################################################

    trunk_rot_w = mathutils.Euler((-np.pi/2, 0,np.pi/2)).to_quaternion()

    limbs_rot_w = mathutils.Euler((np.pi/2, 0, np.pi/2)).to_quaternion()

    foot_rot_w = mathutils.Euler((0,np.pi/2,np.pi)).to_quaternion()


    ############################################################################
    #
    # functional calibration sample range
    #
    ###########################################################################
    for nombre in node_list:
        if nombre in ('neck', 'dorsal', 'base'):
            func_i = func_trunk_i
            func_s = func_trunk_s
        if nombre in ('l-upperarm', 'l-lowerarm', 'l-hand','r-upperarm', 'r-lowerarm', 'r-hand'):
            func_i = func_arms_i
            func_s = func_arms_s
        if nombre in ('l-upperleg', 'l-lowerleg', 'l-foot','r-upperleg', 'r-lowerleg', 'r-foot'):
            func_i = func_legs_i
            func_s = func_legs_s

        # main function to calibrate all nodes
        # here we get the node to calibrate

        node = capture[capture['node_label'] == nombre] # sending only the node to analyze

        # corro las 3 calibraciones (vertical, funcional y quaternion pre
        vert_rot = vert_calib(node, nombre, vert_i, vert_s, PLOT_IMP)
        func_rot, gyro_int = func_calib(node, nombre, vert_rot, func_i, func_s, PLOT_IMP)
        quat_rot = quat_calib(node, nombre, vert_rot, func_rot, gyro_int, vert_i, vert_s, PLOT_IMP)

        # inicializo un empty quat para cargas los quaterniones a usar en cada nodo
        quat_jcs = mathutils.Quaternion()
        # selec jcs_quat accordingly the node
        if nombre in ("base", "dorsal", "neck"):
            quat_jcs = trunk_rot
        elif nombre not in ("l-foot", "r-foot"):
            quat_jcs = limbs_rot
        elif nombre in ("l-foot", "r-foot"):
            quat_jcs = foot_rot


        # compose vert_rot and func_rot and quat_jcs to pos_rot
        pos_rot = vert_rot.inverted() @ func_rot.inverted() @ quat_jcs.inverted()

        # fill DF with calibrations quats...
        df_cali_a.loc[nombre, ['w', 'x', 'y', 'z']] = [vert_rot.w, vert_rot.x, vert_rot.y, vert_rot.z]
        df_cali_b.loc[nombre, ['w', 'x', 'y', 'z']] = [func_rot.w, func_rot.x, func_rot.y, func_rot.z]
        df_cali_c.loc[nombre, ['w', 'x', 'y', 'z']] = [quat_rot.w, quat_rot.x, quat_rot.y, quat_rot.z]
        df_cali_d.loc[nombre, ['w', 'x', 'y', 'z']] = [pos_rot.w, pos_rot.x, pos_rot.y, pos_rot.z]

    #################################################################################################
    # OUTPUT
    #################################################################################################


    # saving output
    df_cali_a.to_csv('out/rotation_vert_{}.csv'.format(OUT_CSV_SUFIX)) # <-------------- OUTPUT FILE - QUATERNIONS VERTICAL CALIBRATION
    df_cali_b.to_csv('out/rotation_func_{}.csv'.format(OUT_CSV_SUFIX)) # <-------------- OUTPUT FILE - QUATERNIONS GYRO CALIBRATION
    df_cali_c.to_csv('out/rotation_pre_{}.csv'.format(OUT_CSV_SUFIX)) # <--------------- OUTPUT FILE - QUATERNIONS PRE CALIBRATION
    df_cali_d.to_csv('out/rotation_pos_{}.csv'.format(OUT_CSV_SUFIX)) # <--------------- OUTPUT FILE - QUATERNIONS PRE CALIBRATION

    #################################################################################################
    # VALIDATION
    #################################################################################################
    #
    # applying rotations to data
    #

    for nodei in node_list:

        node_a = capture[capture['node_label'] == nodei]

        # obtaining acc, giro and mag data from raw csv
        mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z])
        mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z])
        mat_mag = np.array([node_a.m_x, node_a.m_y, node_a.m_z])
        # obtaining quat data from raw csv
        mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z])
        timestamps = np.array(node_a['time(msec)'])

        # transposing
        mat_acc = mat_acc.T
        mat_gyro = mat_gyro.T
        mat_mag = mat_mag.T
        mat_quat = mat_quat.T

        # getting quats from DF
        vert_rot = df_cali_a.loc[nodei]
        func_rot = df_cali_b.loc[nodei]
        quat_rot = df_cali_c.loc[nodei]
        pos_rot = df_cali_d.loc[nodei]

        # generating mathutils quats
        vert_rot = mathutils.Quaternion((vert_rot))
        func_rot = mathutils.Quaternion((func_rot))
        quat_rot = mathutils.Quaternion((quat_rot))
        pos_rot = mathutils.Quaternion((pos_rot))
        
        if OUT_DF_VEC_COORDS in ("JCS", "INITIAL_WORLD"):

            # applying vertical rotation to sensor data
            mat_acc = apply_rot_2mat(mat_acc, vert_rot)
            mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)
            mat_mag = apply_rot_2mat(mat_mag, vert_rot)

            # applying gyro to sensor data
            mat_acc = apply_rot_2mat(mat_acc, func_rot)
            mat_gyro = apply_rot_2mat(mat_gyro, func_rot)
            mat_mag = apply_rot_2mat(mat_mag, func_rot)

            # applying JCS rotation to sensor data so as python data
            # corresponds with blender data
            #
            # selec jcs_quat accordingly the node
            if nodei in ("base", "dorsal", "neck"):
                quat_jcs = trunk_rot
            elif nodei not in ("l-foot", "r-foot"):
                quat_jcs = limbs_rot
            elif nodei in ("l-foot", "r-foot"):
                quat_jcs = foot_rot

            # applying JCS quat
            mat_acc = apply_rot_2mat(mat_acc, quat_jcs)
            mat_gyro = apply_rot_2mat(mat_gyro, quat_jcs)
            mat_mag = apply_rot_2mat(mat_mag, quat_jcs)
            
            if OUT_DF_VEC_COORDS == "INITIAL_WORLD":
                # applying WCS rotation to sensor data FROM JCS so as python data
                # corresponds with WORLD data
                #
                # selec wcs_quat accordingly the node
                if nodei in ("base", "dorsal", "neck"):
                    quat_jcs = trunk_rot_w
                elif nodei not in ("l-foot", "r-foot"):
                    quat_jcs = limbs_rot_w
                elif nodei in ("l-foot", "r-foot"):
                    quat_jcs = foot_rot_w

                # applying JCS quat
                mat_acc = apply_rot_2mat(mat_acc, quat_jcs)
                mat_gyro = apply_rot_2mat(mat_gyro, quat_jcs)
                mat_mag = apply_rot_2mat(mat_mag, quat_jcs)


        elif OUT_DF_VEC_COORDS == "WORLD":
            #applying absolute world rotation of the sensor
            mat_acc = apply_quatmat_2mat(mat_acc, mat_quat, timestamps)
            mat_gyro = apply_quatmat_2mat(mat_gyro, mat_quat, timestamps)
            mat_mag = apply_quatmat_2mat(mat_mag, mat_quat, timestamps)

            #applying pre quat to align in heading with the avatar
            mat_acc = apply_rot_2mat(mat_acc, quat_rot)
            mat_gyro = apply_rot_2mat(mat_gyro, quat_rot)
            mat_mag = apply_rot_2mat(mat_mag, quat_rot)

        
        else:
            raise ValueError("unknown option specified for OUT_DF_VEC_COORDS")


        for i in range(len(mat_quat)):
            quat_i = mathutils.Quaternion((mat_quat[i, :]))
            new_quat = quat_rot @ quat_i  @ pos_rot
            mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]
            
            
        # section to overwrite the raw dataframe with the rotated sensors data
        # getting index from each node
        ref_index = capture[capture['node_label'] == nodei]
        # overwrite sensor data


        capture.loc[ref_index.index, ['a_x', 'a_y', 'a_z']] = mat_acc
        capture.loc[ref_index.index, ['g_x', 'g_y', 'g_z']] = mat_gyro
        capture.loc[ref_index.index, ['m_x', 'm_y', 'm_z']] = mat_mag
        # overwrite quat data
        capture.loc[ref_index.index, ['q_w', 'q_x', 'q_y', 'q_z']] = mat_quat

    #
    # exporting calibrated capture
    #

    # exporting the processed dataframe with rotated sensor data and quats
    capture.to_csv('out/capture_df_calibrated_{}.csv'.format(OUT_CSV_SUFIX)) #<--------- OUTPUT FILE - ORIGINAL CSV FILE WITH SENSOR DATA ROTATED ACCORDINLY CALIBRATION

    # plot all nodes variables calibrated and filtered
    # using plot_var funtion
    if PLOT_CALI_FILTERED:
        label = 'cali + filt + {}'.format(OUT_DF_VEC_COORDS)
        for node_i in node_list:
            if node_i in NODES_TO_PLOT:
                node = capture[capture['node_label'] == node_i]
                plot_var(node_i, node, label)


    pl.show()
