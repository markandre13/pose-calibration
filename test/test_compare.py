import pytest
import numpy as np

def test_vertical(calibrator_instance, capture_data, compare_raw):
    calibrator_instance.vert_calib()
    
    capture, filtered_capture, indexes, node_list = capture_data
    
    for label in node_list:
        node_a = capture[capture['node_label'] == label]
        res = compare_raw.vert_calib(node_a, label, indexes['vert_i'], indexes['vert_s'], False)
        
        calib_res = pytest.approx(tuple(calibrator_instance.results['vert_rot'][label]), abs = 1e-2)
        assert tuple(res) == calib_res 
   
def test_mag_func_heading(calibrator_instance, capture_data, compare_mag_raw):
    calibrator_instance.vert_calib()
    calibrator_instance.mag_func_calib()
    calibrator_instance.heading_calib()

    capture, filtered_capture, indexes, node_list = capture_data
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    for label in node_list:
        
        compare_mag_raw.vert_calib()
        #run and test compare_raw_cali func calib
        compare_mag_raw.mag_mean()
        
        b = compare_mag_raw.df_cali_b
        func_partial = b.loc[label]
        func_rot = calibrator_instance.results["func_rot"][label]
        assert tuple(func_rot) == pytest.approx(tuple(func_partial), abs = 1e-2)

        #run and test compare_raw_cali heading_calib
        compare_mag_raw.jcs_reference()
        compare_mag_raw.rotations_calib()
        
        heading_partial = compare_mag_raw.df_cali_c.loc[label]
        assert tuple(calibrator_instance.results["heading_rot"][label]) == pytest.approx(tuple(heading_partial), abs = 1e-3)
 
def test_func_heading(calibrator_instance, capture_data, compare_raw):
    # execute the module steps up to PCA
    calibrator_instance.vert_calib()
    calibrator_instance.func_calib()
    calibrator_instance.heading_calib()
        
    capture, filtered_capture, indexes, node_list = capture_data
    
    # sort the node_list, the compare_raw_cali procedure requires 'r-upperarm' to be first
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    
    for label in node_list:
        # get the DF for this bone only
        node_a = filtered_capture[filtered_capture['node_label'] == label]
        # get some data from from calibrator_instance
        vert_rot = calibrator_instance.results['vert_rot'][label]
        func_i, func_s = calibrator_instance._get_func_range(label)
        
        #run and test compare_raw_cali func calib
        compare_raw.func_calib(node_a, label, vert_rot, func_i, func_s, False)
        
        func_partial = compare_raw.partials["final_func_rot"][label]
        func_rot = calibrator_instance.results["func_rot"][label]
        #TODO: this comparsion is not working
        if calibrator_instance._intermediate_results['inverted'] == compare_raw.partials['inverted']: 
            assert tuple(func_rot) == pytest.approx(tuple(func_partial), abs = 1e-2)

        #run and test compare_raw_cali heading_calib
        gyro_int = compare_raw.partials["gyro_int"][label]
        compare_raw.quat_calib(node_a, label, vert_rot, func_rot, gyro_int, indexes['vert_i'], indexes['vert_s'], False)
        
        #TODO: this comparsion is not working
        heading_partial = compare_raw.partials["heading_rot"][label]
        assert tuple(calibrator_instance.results["heading_rot"][label]) == pytest.approx(tuple(heading_partial), abs = 1e-3)
