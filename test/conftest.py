import pytest
import pandas as pd
import numpy as np
from pose_calib.defaults import fc, g_s, m_s, a_s

def pytest_addoption(parser):
    parser.addoption("--partial", action="store_true",
                     help="run also the partial tests)")
    
def pytest_runtest_setup(item):
    if 'partial' in item.keywords and not item.config.getoption("--partial"):
        pytest.skip("need --partial option to run this test")

@pytest.fixture
def Calibrator():
    from pose_calib.core import Calibrator
    return Calibrator

def calculate_delta_time(df, vert_i, vert_s):
    deltas = {}
    node_list = df['node_label'].unique()
    
    for node_name in node_list:
        # obtain dt to integrate
        node_df = df[df['node_label'] == node_name]
        # print(node_df.shape)
        node_df = node_df.iloc[vert_i: vert_s]
        [n_samples, n_col] = node_df.shape
        time = node_df['time(msec)'].to_numpy()
        dt_tot = time[1:n_samples] - time[0:n_samples - 1]
        deltas[node_name] = np.mean(dt_tot)
    
    delta_values = list(deltas.values())   
    dts = np.array(delta_values)
    
    assert np.std(dts) < 0.0001, "The calculated delta times do not match"
    return delta_values[0]  
    
@pytest.fixture
def capture_data():
    from raw_algo import stella_ranges
    from pose_calib.core import LP_filter
    from pose_calib.defaults import fc
    from pose_calib.__main__ import ex_csv, ex_json, load_json

    capture = pd.read_csv(ex_csv)
    indexes = load_json(ex_json)['indexes']
    
    filtered_capture = LP_filter(capture, fc, 
                                 calculate_delta_time(capture, indexes['vert_i'], indexes['vert_s']))

    node_list = capture['node_label'].unique()
    
    return capture, filtered_capture, indexes, node_list

@pytest.fixture
def defaults():
    from pose_calib import defaults
    return defaults

@pytest.fixture
def compare_raw():
    from raw_algo import compare_raw_cali
    return compare_raw_cali

@pytest.fixture
def compare_mag_raw(capture_data):
    from raw_algo import mag_calib
    capture, _, indexes, _ = capture_data
    mag = mag_calib.Cali_Mag(capture, g_s, a_s, m_s, indexes['vert_i'], indexes['vert_s'], fc, "test") 
    return mag

@pytest.fixture
def calibrator_instance(Calibrator, capture_data, defaults):
    assert Calibrator
    capture, filtered_capture, indexes, node_list = capture_data
    
    c = Calibrator(capture, defaults.g_s, defaults.a_s, defaults.m_s, indexes, log=False)

    return c
