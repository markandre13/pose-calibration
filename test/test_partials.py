import pytest
import numpy as np

@pytest.mark.partial
def test_PCA(calibrator_instance, capture_data, compare_raw):
    # execute the module steps up to PCA
    calibrator_instance.vert_calib()
    calibrator_instance._PCA()
        
    capture, filtered_capture, indexes, node_list = capture_data
    
    # sort the node_list, the compare_raw_cali procedure requires 'r-upperarm' to be first
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    
    for label in node_list:
        # get the DF for this bone only
        node_a = filtered_capture[filtered_capture['node_label'] == label]
        # get some data from from calibrator_instance
        vert_rot = calibrator_instance.results['vert_rot'][label]
        func_i, func_s = calibrator_instance._get_func_range(label)
        
        #run compare_raw_cali func calib
        compare_raw.func_calib(node_a, label, vert_rot, func_i, func_s, False)
            
        # prepare results to compare
        partial = tuple(compare_raw.partials["PCA"][label])
        calib_res = pytest.approx(tuple(calibrator_instance.results['PCA'][label]), abs=1e-2)
        
        assert partial == calib_res
       
@pytest.mark.partial
def test_mag_mean(calibrator_instance, capture_data, compare_raw):
    # execute the module steps up to PCA
    calibrator_instance.vert_calib()
    calibrator_instance._PCA()
        
    capture, filtered_capture, indexes, node_list = capture_data
    
    # sort the node_list, the compare_raw_cali procedure requires 'r-upperarm' to be first
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    
    mag_mean_compare_calculated = calibrator_instance.legs_nodes + ("r-upperarm",)
    
    for label in mag_mean_compare_calculated:
        calib_mean = calibrator_instance._get_mag_mean(label)

        # get the DF for this bone only
        node_a = filtered_capture[filtered_capture['node_label'] == label]
        # get some data from from calibrator_instance
        vert_rot = calibrator_instance.results['vert_rot'][label]
        func_i, func_s = calibrator_instance._get_func_range(label)
        
        #run compare_raw_cali func calib
        compare_raw.func_calib(node_a, label, vert_rot, func_i, func_s, False)

        partial = tuple(compare_raw.partials["mag_mean"][label])
        calib_res = pytest.approx(tuple(calib_mean))
        
        assert partial ==  calib_res
        
@pytest.mark.partial
def test_compare_sign(calibrator_instance, capture_data, compare_raw):
    # execute the module steps up to PCA
    calibrator_instance.vert_calib()
    calibrator_instance._PCA()
        
    capture, filtered_capture, indexes, node_list = capture_data
    
    # sort the node_list, the compare_raw_cali procedure requires 'r-upperarm' to be first
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    
    #set the global body mag_ref first  
    calibrator_instance._get_mag_mean('r-upperarm', set_body_ref=True)
    
    for label in calibrator_instance.legs_nodes:
        gyro_int = calibrator_instance._compare_sign(label)

        # get the DF for this bone only
        node_a = filtered_capture[filtered_capture['node_label'] == label]
        # get some data from from calibrator_instance
        vert_rot = calibrator_instance.results['vert_rot'][label]
        func_i, func_s = calibrator_instance._get_func_range(label)
        
        #run compare_raw_cali func calib
        compare_raw.func_calib(node_a, label, vert_rot, func_i, func_s, False)

        partial = compare_raw.partials["gyro_int"][label]
 
        assert gyro_int[1] == pytest.approx(partial[1], abs = 1e-4)

@pytest.mark.partial
def test_gyro_integral(calibrator_instance, capture_data, compare_raw):
    # execute the module steps up to PCA
    calibrator_instance.vert_calib()
    calibrator_instance._PCA()
        
    capture, filtered_capture, indexes, node_list = capture_data
    
    # sort the node_list, the compare_raw_cali procedure requires 'r-upperarm' to be first
    node_list = list(node_list)
    node_list.sort(key = lambda n: 'r-upperarm' not in n)
    
    for label in set(calibrator_instance.arms_nodes + calibrator_instance.trunk_nodes) - {'r-upperarm'}:
        gyro_int = calibrator_instance._calculate_gyro_integral(label)

        # get the DF for this bone only
        node_a = filtered_capture[filtered_capture['node_label'] == label]
        # get some data from from calibrator_instance
        vert_rot = calibrator_instance.results['vert_rot'][label]
        func_i, func_s = calibrator_instance._get_func_range(label)
        
        #run compare_raw_cali func calib
        compare_raw.func_calib(node_a, label, vert_rot, func_i, func_s, False)

        partial_gint = compare_raw.partials["gyro_int"][label]
        # partial_frot = compare_raw.partials["final_func_rot"][label]

        assert gyro_int[1] == pytest.approx(partial_gint[1], abs = 1e-4)
