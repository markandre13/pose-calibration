# Chordata pose (segment) calibration

The main sensor to segment calibration proceduce to be used withing the Chordata Motion capture framework.
It uses an static and functional calibration to map the orientation of 15 or 17 sensors mounted on the body of the performer to the defatul Chordata JCS (Joint coordinate system)

![Chordata JCS](JCS/Chordata_default_biped_JSC_v1-axonometric-sm.png)

## Calibration sequence

This calibration algorithm requires motion capture data from a person doing the following movements:

- **Static:** Stand still at N-POSE
- **Functional:** Raise both arms
- **Functional:** Lower the trunk as in a reverence
- **Functional:** Raise one leg at a time keeping them as rigid as possible 

For a complete description of the movements and proceduce take a look at [this chapter of our docs](https://chordata.gitlab.io/docs/blender-capture/#pose-calibration)


## Development setup

The following instructions will allow you to install and run the algorithm with pre-recorded data:
- A `CSV` file with the dump of the capture, including RAW sensor data as well as Quaternion orientations for each sensor
- A `JSON` file with the indexes of the CSV to be used for each section (static, functional arms, etc)

### Install with venv

```bash
python -m venv venv  
venv/bin/pip install -e .
venv/bin/pip install 'chordata-pose-calib[test]'
venv/bin/pip install 'chordata-pose-calib[dev]'
```

### Run from command line:

```
venv/bin/pose-calib [options]
```
Run `venv/bin/pose-calib -h` to get the available options

### Run tests

```
venv/bin/pytest
```