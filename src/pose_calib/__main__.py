# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pandas as pd
import json
import pathlib
import argparse
import re

from .core import Calibrator
from .defaults import g_s, a_s, m_s

def_take = "Chordata_calib"
ex_csv  = pathlib.Path(__file__).parent.joinpath('test_data/{}_dump.csv'.format(def_take))
ex_json = pathlib.Path(__file__).parent.joinpath('test_data/{}_data.json'.format(def_take))
def_csv  = pathlib.Path('./{}_dump.csv'.format(def_take))
def_json = pathlib.Path('./{}_data.json'.format(def_take))


def load_json(f = def_json):
    p = pathlib.Path(f)
    if not p.is_file():
        print("Indexes JSON file: `{}` not found, exiting".format(p.resolve(())))
        quit()
        
    with open(f) as file:
        ranges = json.load(file)
        
    print("Indexes JSON file: `{}` loaded".format(p.resolve()))
    return ranges

def load_csv(p = pathlib.Path(def_csv)):
    f = p.resolve()
    if not p.is_file():
        print("Take dump CSV file: `{}` not found, exiting".format(p.resolve()))
        quit()
    
    df = pd.read_csv(f)
    print("Take dump CSV file: `{}` loaded".format(p.resolve()))
    
    return df


parser = argparse.ArgumentParser(description="Chordata's pose (segment) calibration")
parser.add_argument("-t","--take", type=pathlib.Path, default=None,
                    help="path to the .csv file containing a dump of the motion capture take")

parser.add_argument("-i","--indexes", type=load_json, default=None,
                    help="path to the .json file containing the indexes for each calib phase")

parser.add_argument("-k","--key", type=str, default="indexes",
                    help="key inside the json where the indexes are located")

parser.add_argument("-l","--log", type=str, default="./calib.log",
                    help="file where to log, set empty to do a quiet run")

parser.add_argument("--basename", type=str, default=None,
                    help="The basename for output files. Defaults to --take filename without extension")

parser.add_argument("-o","--out", type=pathlib.Path, default="./out",
                    help="Directory where to create the output files")

parser.add_argument("--example", action='store_true',
                    help="Run an example calibratio with pre-saved data")

parser.add_argument("--show-plots", action='store_true',
                    help="Show plots after running the calibration")

parser.add_argument('--plot-nodes', nargs='+', default=['r-lowerarm','neck', 'r-upperleg', 'l-upperleg'],
    help='List of nodes to plot')

parser.add_argument("--save-quats", action='store_true',
                    help="Save calibration quaternions as csv file")

parser.add_argument("--save-applied", action='store_true',
                    help="Save CSV with applied calibration")

parser.add_argument("--save-data", action='store_true',
                    help="Save calibration process data as JSON")

def main():
    args = parser.parse_args()

    if args.example:
        take = ex_csv
        capture = load_csv(ex_csv)
    else:
        take = def_csv if args.take is None else args.take
        capture = load_csv(take)

    if args.basename is None:
        basename = re.sub(r".\w{3,}$", "", take.resolve().name)
    else:
        basename = args.basename


    try:
        if args.example:
            indexes = load_json(ex_json)[args.key]
        else:
            indexes = load_json()[args.key] if args.indexes is None else args.indexes[args.key]
    except KeyError as e:
        print("Key {} not found in the indexes JSON file, exiting".format(e))
        quit()

    calib_1 = Calibrator(
        capture,
        g_s, a_s, m_s,
        indexes,
        log=args.log
    )

    calib_1.run()

    if args.save_quats:
        calib_1.save_calib_quats(basename, location=args.out)

    if args.save_applied:
        calib_1.apply(basename, location=args.out)
    
    if args.save_data:
        calib_1.dump_calib_data(basename, location=args.out)

    if args.show_plots:
        calib_1.plot('world', args.plot_nodes)
        calib_1.show_plots()


if __name__ == "__main__":
    main()
