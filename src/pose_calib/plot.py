# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import numpy as np
from enum import Flag, auto

try:
    import matplotlib
    matplotlib.use('Qt5Agg')
    import matplotlib.pyplot as pl
except Exception:
    pl = None

def int_analysis(node_name, df):
    if not pl:
        return
    
    # print("**********")
    # print(node_name, df)
    
    fig = pl.figure('range for gyro_int ' + node_name)
    ax = fig.add_subplot(111)
    ax.set_title('range for gyro_int  ' + node_name, y=0.9)
    ax.plot(df[:,0], c='r', alpha=0.5)
    ax.plot(df[:,1], c='g', alpha=0.5)
    ax.plot(df[:,2], c='b', alpha=0.5)
    pl.xlabel('relative sample')
    pl.ylabel('gyro \n  [deg/sec]')
    pl.legend(('x','y','z'))
    ax.grid()
    
def pca_analysis(node_name, df, eigen_vectors, sing_values, angle_deg):
    if not pl:
        return
    
    fig = pl.figure('analysis ' + node_name)
    ax = fig.add_subplot(111)
    ax.set_title('giro analysis ' + node_name, y=0.9)
    ax.plot(df[:, 0], df[:, 1], 'k.')
    sv = sing_values
    ax.plot([0, eigen_vectors[0][0] * sv[0]], [0, eigen_vectors[1][0] * sv[0]], 'm')
    ax.plot([0, eigen_vectors[0][1] * sv[1]], [0, eigen_vectors[1][1] * sv[1]], 'y')
    ax.text(eigen_vectors[0][0] * sv[0] / 2, eigen_vectors[1][0] * sv[0] / 2, '%.2f' % angle_deg, fontdict=None)
    pl.xlabel('x')
    pl.ylabel('y')
    ax.set_aspect('equal')
    ax.grid()
    
#
# fuction to plot df variables
# input data in the node_name and the data frame from where
# de acc, gyro and mag variables are read
#
def gyro_acc_mag(node_name, df, label, a_s, m_s, g_s):
    if not pl:
        return
        
    [n_samples, n_col] = df.shape
    samples = np.arange(n_samples)
    pl.figure(node_name + ' ' + label)
    pl.suptitle(node_name + ' ' + label, size=18)
    # acc
    ax1 = pl.subplot(3,3,1)
    pl.plot(samples, df.a_x * a_s, 'r')
    pl.ylabel('Acceleration \n [g]', multialignment='center')
    pl.gca().set_title('X')
    pl.grid()
    pl.subplot(3,3,2, sharex=ax1)
    pl.plot(samples, df.a_y * a_s, 'g')
    pl.gca().set_title('Y')
    pl.grid()
    pl.subplot(3,3,3, sharex=ax1)
    pl.plot(samples, df.a_z * a_s, 'b')
    pl.gca().set_title('Z')
    pl.grid()
    # gyro
    pl.subplot(3,3,4, sharex=ax1)
    pl.plot(samples, df.g_x * g_s, 'r')
    pl.ylabel('Angular vel \n [deg/seg]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,5, sharex=ax1)
    pl.plot(samples, df.g_y * g_s, 'g')
    pl.grid()
    pl.subplot(3,3,6, sharex=ax1)
    pl.plot(samples, df.g_z * g_s, 'b')
    pl.grid()
    # mag
    pl.subplot(3,3,7, sharex=ax1)
    pl.plot(samples, df.m_x * m_s, 'r')
    pl.ylabel('Mag field \n [Gauss]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,8, sharex=ax1)
    pl.plot(samples, df.m_y * m_s, 'g')
    pl.grid()
    pl.subplot(3,3,9, sharex=ax1)
    pl.plot(samples, df.m_z * m_s, 'b')
    pl.grid()

def gyro_index_analysis(signal, signal_ma, percent, in_inf, in_sup, node_name):
    if not pl:
        return

    max = np.max(signal_ma)
    pl.figure(f'gyro index finding node: {node_name}')
    pl.plot(signal)
    pl.plot(signal_ma)
    pl.plot(np.array(signal_ma > max * percent) * max)
    pl.text(in_inf, 0, f'{in_inf}')
    pl.text(in_sup, 0, f'{in_sup}')
    pl.xlabel('relative sample')
    pl.ylabel('deg per seconds')
    pl.title(f'gyro index finding node: {node_name}')
    pl.grid()
    pl.legend(['original', 'mov-averaged', f"signal > than {percent*100}%"])

def show_plots():
    if not pl:
        return

    pl.show()