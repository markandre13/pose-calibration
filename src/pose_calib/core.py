# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pandas as pd
import numpy as np
import mathutils

from warnings import warn
from pathlib import Path
from datetime import datetime

from .utils import (node_filter, apply_rot_2mat, apply_quat_2node, 
                    Output, get_JCS_orientations, apply_quatmat_2mat)
from . import defaults
from . import plot

# HELPER CLASES FOR mathutils formatting
# Note: mind the space before the float precision in the format string. 
#       It leaves a space for a positive number where the minus sign is for nagetives ones.
#       Thit helps aligning values

class _Quaternion(mathutils.Quaternion):
    def __str__(self):
        return "Quat({: .4f}, {: .4f}, {: .4f}, {: .4f})".format(self.w,self.x,self.y,self.z)

class _Vector(mathutils.Vector):
    def __str__(self):
        return "Vec({: .4f}, {: .4f}, {: .4f})".format(self.x,self.y,self.z)
    
    def rotation_difference(self, other):
        q = super().rotation_difference(other)
        return _Quaternion(q)

# end HELPER CLASES FOR mathutils formatting

def LP_filter(df, fc, delta_time):
    node_list = df['node_label'].unique()
    df = df.copy()

    for node_name in node_list:
        node = df[df['node_label'] == node_name] 
        node = node_filter(node, fc, 3, delta_time)
        df.loc[df['node_label'] == node_name, node.columns] = node
    
    return df


class Base_Calibrator(defaults.JCS_Nodes):
    required_columns = ('time(msec)','node_label','q_w', 'q_x', 'q_y', 'q_z',
                        'g_x', 'g_y', 'g_z', 'a_x', 'a_y', 'a_z', 'm_x', 'm_y', 'm_z')
    
    required_indexes = ('vert_i', 'vert_s')
    optional_indexes = (
        'func_arms_i',
        'func_arms_s',
        'func_trunk_i',
        'func_trunk_s',
        'func_legs_r_i',
        'func_legs_r_s',
        'func_legs_l_i',
        'func_legs_l_s'
    )

    def __init__(self, input_df, g_s, a_s, m_s, section_indexes, fc = defaults.fc, 
                        log = False, adjust_nodes = {}):
        # ------------ input_df validation ------------ 
        if not isinstance(input_df, pd.DataFrame):
            raise ValueError("The input of the pose Calibrator should be a pandas.DataFrame")
        
        columns = set(input_df.columns.to_list())
        
        missing_keys = set(self.required_columns) - columns
        if missing_keys:
            raise ValueError("The input DataFrame of the pose Calibrator is missing the following colums {}".format( tuple(k for k in self.required_columns if k in missing_keys) ))
        
        # ------------  setup logger ------------
        log_file = log if isinstance(log, str) else None 
        self.out = Output.get_instance(log, log_file)
        self.out.log("Chordata pose calibration v{} init".format(defaults.__version__), banner=True)
        self.now_str = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
        self.quats_fmt = '{_now_str}_rotation_{_rotation_type}.csv'
        self.apply_fmt = '{_now_str}_calibrated_{_save_as}.csv'
        self.json_fmt = '{_now_str}_{_save_as}_data.json'

        self.out.log("Start @", self.now_str)
        
        # ------------  section_indexes validation ------------ 
        k_indexes = section_indexes.keys()

        processed_section_indexes = {}

        for index in self.required_indexes:
            if index not in k_indexes:
                raise ValueError(f"Missing '{index}' key in section_indexes")
            elif not isinstance(section_indexes[index], int):
                raise TypeError(f"Wrong type for section_indexes['{index}']")
            processed_section_indexes[index] = section_indexes[index]
        
        for index in self.optional_indexes:
            if index not in k_indexes:
                processed_section_indexes[index] = -1
            elif isinstance(section_indexes[index], int):
                processed_section_indexes[index] = section_indexes[index]
            else:
                raise TypeError(f"Wrong type for section_indexes['{index}']")

        self.raw_section_indexes = section_indexes.copy()
        self.section_indexes = processed_section_indexes

        # ------------  pre-process df ------------ 
        self.node_list = input_df['node_label'].unique()
        self.dt = self.calculate_delta_time(input_df)
        
        sensor_cols = ["{}_{}".format(s,a) for s in ['g','a','m'] for a in ['x', 'y', 'z']]
        self.input_df = input_df.astype(dtype={col:"float64" for col in sensor_cols})
        self.fc = fc
        filtered_df = LP_filter(input_df, fc, self.dt)
        self.input_node_dfs = {}
        self.filtered_node_dfs = {}
        for node_name in self.node_list:
            self.input_node_dfs[node_name] = input_df[input_df['node_label'] == node_name]
            self.filtered_node_dfs[node_name] = filtered_df[filtered_df['node_label'] == node_name]
        
        self.out.log("Labels in DF: {}".format(self.node_list))
        self.check_nodes_in_df(self.node_list)

        # --------------- set RAW to real conversion factors -------------
        self.g_s = g_s
        self.a_s = a_s
        self.m_s = m_s

        # --------------- Get the JCS rotations -------------
        self.JCS = get_JCS_orientations(self.node_list)

        # ------------  create empty structures to be filled ------------ 
        self._intermediate_results = {}
        self.results = {}

        # ------------ section functional indexes adjustment ------
        self.adjust_func_ranges(adjust_nodes)
        # ---- end __init__----

    ### ============ utility methods ============

    def adjust_func_ranges(self, nodes = {}):
        default_nodes = {
            'arms': 'r-upperarm',
            'r-leg': 'r-upperleg',
            'l-leg': 'l-upperleg',
            'trunk': 'neck'
        }
        default_nodes.update(nodes)
        
        self.out.log('Functional calibration ranges', title=True)
        # run only for 1 member of each node region
        for region in default_nodes.keys():
            node_name = default_nodes[region]
            func_i, func_s = self._get_func_range(node_name)
            # obtain gyro data from df
            df = self.filtered_node_dfs[node_name]
            df = df.iloc[func_i:func_s]
            data = df[['g_x', 'g_y', 'g_z']].to_numpy()
            # scale data
            data = data * self.g_s
            # find axis with greatest gyro to process
            row_i, col_i = np.unravel_index(np.argmax(data), data.shape)
            r_inf, r_sup = self._adjust_index(data[:, col_i], node_name = node_name)
            # here we create a new set of variables func_noderegion_adjusted_i/s
            # to read during functional calibration
            if node_name in self.trunk_nodes:
                inf = self.section_indexes['func_trunk_i'] + r_inf
                sup = self.section_indexes['func_trunk_i'] + r_sup
                self.section_indexes['func_trunk_ad_i'] = inf
                self.section_indexes['func_trunk_ad_s'] = sup
                self.out.log(
                    f'Trunk: from {inf} to {sup}'
                )
            elif node_name in self.arms_nodes:
                inf = self.section_indexes['func_arms_i'] + r_inf
                sup = self.section_indexes['func_arms_i'] + r_sup
                self.section_indexes['func_arms_ad_i'] = inf
                self.section_indexes['func_arms_ad_s'] = sup
                self.out.log(
                    f'Arms from {inf} to {sup}'
                )
            elif node_name in self.left_leg_nodes:
                inf = self.section_indexes['func_legs_l_i'] + r_inf
                sup = self.section_indexes['func_legs_l_i'] + r_sup
                self.section_indexes['func_legs_l_ad_i'] = inf
                self.section_indexes['func_legs_l_ad_s'] = sup
                self.out.log(
                    f'Left leg from {inf} to {sup}'
                )
            elif node_name in self.right_leg_nodes:
                inf = self.section_indexes['func_legs_r_i'] + r_inf
                sup = self.section_indexes['func_legs_r_i'] + r_sup
                self.section_indexes['func_legs_r_ad_i'] = inf
                self.section_indexes['func_legs_r_ad_s'] = sup
                self.out.log(
                    f'Right leg from {inf} to {sup}'
                )

    def _adjust_index(self, signal, ma=20, percent=0.1, node_name='node_name'):
        signal_ma = np.convolve(np.abs(signal), np.ones(ma), 'same') / ma
        max = np.max(signal_ma)
        indexes = np.nonzero(np.array(signal_ma > max * percent))
        in_inf = indexes[0][0]
        in_sup = indexes[0][-1]

        plot_values = (signal, signal_ma, percent, in_inf, in_sup)
        try:
            self._intermediate_results['GYRO_INDEX'][node_name] = plot_values
        except KeyError:
            self._intermediate_results['GYRO_INDEX'] = { node_name: plot_values }

        return in_inf, in_sup

    def calculate_delta_time(self, df):
        deltas = {}
        
        for node_name in self.node_list:
            # obtain dt to integrate
            node_df = df[df['node_label'] == node_name]
            node_df = node_df.iloc[self.section_indexes['vert_i']:self.section_indexes['vert_s']]
            [n_samples, n_col] = node_df.shape
            time = node_df['time(msec)'].to_numpy()
            dt_tot = time[1:n_samples] - time[0:n_samples - 1]
            deltas[node_name] = np.mean(dt_tot)
        
        delta_values = list(deltas.values())   
        dts = np.array(delta_values)
        
        assert np.std(dts) < 0.0001, "The calculated delta times do not match"
        return delta_values[0]            

    # function to get the functional ranges adjusted
    def _get_func_range_ad(self, node_name):
        try:
            if node_name in self.trunk_nodes:
                func_i = self.section_indexes['func_trunk_ad_i']
                func_s = self.section_indexes['func_trunk_ad_s']
            elif node_name in self.arms_nodes:
                func_i = self.section_indexes['func_arms_ad_i']
                func_s = self.section_indexes['func_arms_ad_s']
            elif node_name in self.left_leg_nodes:
                func_i = self.section_indexes['func_legs_l_ad_i']
                func_s = self.section_indexes['func_legs_l_ad_s']
            elif node_name in self.right_leg_nodes:
                func_i = self.section_indexes['func_legs_r_ad_i']
                func_s = self.section_indexes['func_legs_r_ad_s']
            else:
                raise ValueError(f'Invalid node name: {node_name}')
            return func_i, func_s
        except AttributeError:
            raise LookupError(f'No functional calibration range found for node: {node_name}')

         # here it would be usefull to print this ranges in the log file

    def _get_func_range(self, node_name):
        try:
            if node_name in self.trunk_nodes:
                func_i = self.section_indexes['func_trunk_i']
                func_s = self.section_indexes['func_trunk_s']
            elif node_name in self.arms_nodes:
                func_i = self.section_indexes['func_arms_i']
                func_s = self.section_indexes['func_arms_s']
            elif node_name in self.right_leg_nodes:
                func_i = self.section_indexes['func_legs_r_i']
                func_s = self.section_indexes['func_legs_r_s']
            elif node_name in self.left_leg_nodes:
                func_i = self.section_indexes['func_legs_l_i']
                func_s = self.section_indexes['func_legs_l_s']
            else:
                raise ValueError(f'Invalid node name: {node_name}')
            return func_i, func_s
        except AttributeError:
            raise LookupError(f'No functional calibration range found for node: {node_name}')
  
    @classmethod
    def sta_correction(cls, gamma, gyro_int, factor):
        return _Quaternion((0, 0, 1), - gamma) @ mathutils.Euler((0, 0, gyro_int[2] * np.pi/180*factor)).to_quaternion()
        
    def _assert_run(self, msg):
        assert "post_rot" in self.results.keys(), "`run` should be executed " + msg
        
    def show_plots(self):
        plot.show_plots()

    ### ============ output secction for off-line tests =======
  
    def save_calib_quats(self, take, location = '.'):
        self._assert_run("before saving calibration rotations")

        folder = Path(location)
        folder.mkdir(exist_ok = True)
        self.out.log("Save CSV", banner=True)
        self.out.log(f'Saving calibration rotations CSV in:\n\t {folder.resolve()}')

        for rotation_type, result_index in [
            ('vert', 'vert_rot'),
            ('func', 'func_rot'),
            ('pos', 'post_rot'),
            ('pre', 'heading_rot')
        ]:
            path = folder / self.quats_fmt.format(_now_str=self.now_str, 
                                                _rotation_type = rotation_type,
                                                _save_as=take)

            with path.open('w') as f:
                f.write('node_label,w,x,y,z\n')
                for key in self.node_list:
                    quat = self.results[result_index][key]
                    f.write(f'{key},{quat[0]},{quat[1]},{quat[2]},{quat[3]}\n')

            self.out.log(f'Saved {rotation_type} rotations in:\n\t {path.resolve()}')


    ### ============ ploting data section ============
    def _get_plot_node_initial_rotation(self, node_name, stage, filtered=True):
        if 'unfiltered' in stage:
            filtered = False
                    
        try:
            if filtered is True:
                node_a = self.filtered_node_dfs[node_name].copy()
            else:
                node_a = self.input_node_dfs[node_name].copy()
                if filtered is not False and isinstance(filtered, (int, float)):
                    node_a = node_filter(node_a, fc=filtered, dt=self.dt)
                    
        except KeyError:
            return None

        if 'raw' in stage or stage == 'world':
            return node_a

        # apply vert
        vert_rot = self.results["vert_rot"][node_name]
        node_a = apply_quat_2node(node_a, vert_rot)
        if stage == 'vertical':
            return node_a

        # get functional rotation
        func_rot = self.results["func_rot"][node_name]
        node_a = apply_quat_2node(node_a, func_rot)
        if stage in ('functional'):
            return node_a

        # get JCS rotation
        quat_jcs = self.JCS[node_name]
        node_a = apply_quat_2node(node_a, quat_jcs.inverted())
        if stage in ('jcs'):
            return node_a

    def _get_plot_node_rotation(self, node_name, stage, filtered=True):
        stage = stage.lower()

        initial_filtered = False if stage == 'world' else filtered

        node_a = self._get_plot_node_initial_rotation(node_name, stage, initial_filtered)

        if stage != 'world':
            return node_a

        #applying absolute world rotation of the sensor
        mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z]).T

        mat_acc =  np.array([node_a.a_x, node_a.a_y, node_a.a_z]).T
        mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z]).T
        mat_mag =  np.array([node_a.m_x, node_a.m_y, node_a.m_z]).T

        mat_acc =  apply_quatmat_2mat(mat_acc, mat_quat)
        mat_gyro = apply_quatmat_2mat(mat_gyro, mat_quat)
        mat_mag =  apply_quatmat_2mat(mat_mag, mat_quat)

        node_a.loc[:, ['a_x', 'a_y', 'a_z']] = mat_acc
        node_a.loc[:, ['g_x', 'g_y', 'g_z']] = mat_gyro
        node_a.loc[:, ['m_x', 'm_y', 'm_z']] = mat_mag

        #applying pre quat to align in heading with the avatar
        heading_rot = self.results["heading_rot"][node_name]
        node_a = apply_quat_2node(node_a, heading_rot)
        
        if filtered is not False and isinstance(filtered, (int, float)):
            node_a = node_filter(node_a, fc=filtered, dt=self.dt)

        return node_a

    def _fitered_str(self, filtered):
        if filtered:
            f = float(filtered)
            return " LP fc={:.2f}".format(f) if filtered is not True else " LP fc={:.2f}".format(self.fc)
        return ' no LP filter'
        

    def plot(self, _stage, nodes = defaults.body_nodes, filtered=True):
        self._assert_run("before plotting")
        
        # check if stage is a valid one (case insensitive)
        stage = _stage.lower()
        if 'unfiltered' in stage: stage = 'unfiltered_raw' 
        if stage not in [
            'gyro_index',
            'pca',
            'gyro_int',
            'unfiltered_raw',
            'raw',
            'vertical',
            'functional',
            'jcs',
            'world'
        ]:
            warn(f'{_stage} is not a valid stage to plot', stacklevel=2)
            return

        # validate RUN first
        # for all specified nodes, rotate and plot the selected stage
        for node_name in nodes:
            self.out.log(f"Plotting {stage.upper()} for {node_name}")
            eventual_warning_text = f'{node_name} has no data to plot for {stage}'
            if stage == 'gyro_index':
                try:
                    signal, signal_ma, percent, in_inf, in_sup = self._intermediate_results['GYRO_INDEX'][node_name]
                except KeyError:
                    warn(eventual_warning_text, stacklevel=2)
                    continue
                plot.gyro_index_analysis(
                    signal, signal_ma, percent, in_inf, in_sup, node_name
                )
            elif stage == 'pca':
                try:
                    datos, eigen_vectors, sing_values, angle_deg = self._intermediate_results['PCA'][node_name]
                except KeyError:
                    warn(eventual_warning_text, stacklevel=2)
                    continue
                plot.pca_analysis(node_name, datos, eigen_vectors, sing_values, angle_deg)
            elif stage == 'gyro_int':
                try:
                    mat_gyro, = self._intermediate_results['GYRO_INT'][node_name]
                except KeyError:
                    warn(eventual_warning_text, stacklevel=2)
                    continue
                plot.int_analysis(node_name, mat_gyro)
            else:
                node_a = self._get_plot_node_rotation(node_name, stage, filtered)
                if node_a is None:
                    warn(eventual_warning_text, stacklevel=2)
                    continue
                
                plot.gyro_acc_mag(
                    node_name, node_a,
                    {
                        'raw': 'Raw',
                        'unfiltered_raw': 'Raw',
                        'vertical': 'Vertical',
                        'functional': 'Functional',
                        'jcs': 'Calibrated (in JCS)',
                        'world': 'Calibrated (in WORLD)'
                    }[stage] + self._fitered_str(filtered),
                    self.a_s, self.m_s, self.g_s
                )
    
    def apply(self, save_as=None, location = '.', stage = 'world', filtered=False):
        self._assert_run("before applying calibration to raw DF")
        
        node_list = self.node_list
        dest_df = self.input_df.copy()
        vert_i = self.section_indexes['vert_i']
        
        self.out.log(' ==== APPLYING CALIBRATION TO RAW DF ==== ', banner=True)
        self.out.log("Filter:" + self._fitered_str(filtered))
        
        for node_name in self.node_list:
            self.out.log('Applying Calibration to {}'.format(node_name))

            node_a = self._get_plot_node_rotation(node_name, stage, filtered)
            
            # getting quats from previous result
            pre_rot = self.results['heading_rot'][node_name]
            pos_rot = self.results['post_rot'][node_name]
            
            # generating mathutils quats
            pre_rot = _Quaternion((pre_rot))
            pos_rot = _Quaternion((pos_rot))
      
            # applying rotation to quat data
            mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z])
            mat_quat = mat_quat.T
            for i in range(len(mat_quat)):
                quat_i = _Quaternion((mat_quat[i, :]))
                new_quat = pre_rot @ quat_i  @ pos_rot
                mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]
                
            # section to overwrite the raw dataframe with the rotated sensors data
            # getting index from each node
            ref_index = dest_df[dest_df['node_label'] == node_name]
            
            # obtaining acc, giro and mag data from calibrated
            mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z], dtype=np.int16).T
            mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z], dtype=np.int16).T
            mat_mag = np.array([node_a.m_x, node_a.m_y, node_a.m_z], dtype=np.int16).T
            
            # overwrite sensor data
            dest_df.loc[ref_index.index, ['a_x', 'a_y', 'a_z']] = mat_acc
            dest_df.loc[ref_index.index, ['g_x', 'g_y', 'g_z']] = mat_gyro
            dest_df.loc[ref_index.index, ['m_x', 'm_y', 'm_z']] = mat_mag
            # overwrite quat data
            dest_df.loc[ref_index.index, ['q_w', 'q_x', 'q_y', 'q_z']] = mat_quat

            
        if isinstance(save_as, str):
            folder = Path(location)
            folder.mkdir(exist_ok = True)
            path = folder / self.apply_fmt.format(_now_str=self.now_str, _save_as=save_as)
            dest_df.to_csv(str(path))
            self.out.log(f'Saved calibrated DF in {path}')
            
        return dest_df
        
        
    def dump_calib_data(self, save_as=None, location = '.'):
        self._assert_run("before saving calibration rotations")
        
        if not isinstance(save_as, str):
            self.out.log(f'Calibration data JSON not saved, please provide a valid basename')
            return
        
        import copy
        import json

        _out = {"calib_results": copy.deepcopy(self.results), "indexes": {}}
        
        def convert_to_tuples(main_key):
            for k1,val1 in self.results.items():
                if isinstance(val1, dict):
                    for k2,val2 in self.results[k1].items():
                        prev_type = type(val2)
    
                        if isinstance(val2, (mathutils.Quaternion, np.ndarray)):
                            _out[main_key][k1][k2] = tuple(val2)
                        
        
        convert_to_tuples("calib_results")

        for k,i in self.section_indexes.items():
            _out["indexes"][k] = int(i)
        
        folder = Path(location)
        folder.mkdir(exist_ok = True)
        path = folder / self.json_fmt.format(_now_str=self.now_str, _save_as=save_as)

        # write out file
        out_str = json.dumps(_out, indent=4)
        with open(path, 'w') as file:
            file.write(out_str)

        self.out.log(f'Saved calibration data in {path}')

    def run(self):
        self.out.log('RUNNING VERTICAL CALIBRATION', banner=True)
        self.vert_calib()
        self.out.log('RUNNING FUNCTIONAL CALIBRATION', banner=True)
        self.mag_func_calib()
        self.out.log('RUNNING HEADING CALIBRATION', banner=True)
        self.heading_calib()

        self.results["post_rot"] = {}
        for node_name in self.node_list:
            vert_rot = self.results["vert_rot"][node_name]
            func_rot = self.results["func_rot"][node_name]

            quat_jcs = self.JCS[node_name]
            post_quat = vert_rot.inverted() @ func_rot.inverted() @ quat_jcs
            self.results["post_rot"][node_name] = post_quat
            
    ### ============ vertical calibration ============        

    def _node_vert_calib(self, df):
        # assert df has only one label
        if df['node_label'].unique().size != 1:
            raise ValueError("The Node_Calibrator._node_vert_calib function should receive a DataFrame with only one node.\
Received: {}".format(df['node_label'].unique().tolist()))
            
        node_name = df['node_label'].unique()[0]
          
        # VERTICAL CALIBRATION
        self.out.log('Vertical calibration, node:', node_name, title=True)
        
        # reading steady state data from df
        node_a = df.iloc[self.section_indexes['vert_i']:self.section_indexes['vert_s']]
        mat_acc = np.array([node_a.a_x, node_a.a_y, node_a.a_z] ) * self.a_s
        # get mean
        acc_mean = np.mean(mat_acc, axis=1)
        self.out.log('acc vector mean:', _Vector(acc_mean))
        vec1 = _Vector((acc_mean))
        # generate reference down vector
        down = _Vector((0, 0, -1))  # gravity value is negative
        # get rotation
        vert_rot = vec1.rotation_difference(down)
        # rotate vector
        vec1.rotate(vert_rot)
        self.out.log('vert_rot - math utils:', vert_rot)
        self.out.log('acc mean vector vert rotated:', vec1)

        return vert_rot
    
    def vert_calib(self):
        self.results["vert_rot"] = {}
        for node_name in self.node_list:
            node_df = self.filtered_node_dfs[node_name]
            self.out.log("VERT CALIB for", node_name, title=True)
            self.results["vert_rot"][node_name] = self._node_vert_calib(node_df)
            
        return self.results["vert_rot"]
    
    def mag_func_calib(self):
        assert "vert_rot" in self.results.keys(), "The vert_calib should be ran before the func_calib"
        self.results['func_rot'] = {}
        mag_all_mean = pd.DataFrame(index =self.node_list, columns =['x', 'y', 'z'])
        self.out.log('GETTING MAGNETIC MEAN FROM K-CEPTORS USIGN VERT_ROT', title=True)
        for node_analysis in self.node_list:
            self.out.log('-' * 84)
            self.out.log('magnetic mean ' + node_analysis)
            self.out.log('-' * 84)
            # reading steady state data from df
            df = self.filtered_node_dfs[node_analysis]
            node_a = df.iloc[self.section_indexes["vert_i"]:self.section_indexes["vert_s"]]
            mat_acc = np.array([node_a.m_x, node_a.m_y, node_a.m_z] )* self.m_s
            # get mean
            mag_mean = np.mean(mat_acc, axis=1)
            self.out.log('mag vector mean', mag_mean)
            # getting vert_rot from df
            vert_rot = self.results['vert_rot'][node_analysis]
            # converting to quat
            vert_rot = mathutils.Quaternion((vert_rot))
            # addind 1 dim to mag data
            mag_mean = np.expand_dims(mag_mean, axis=0)
            # apply vert rotation
            mag_mean = apply_rot_2mat(mag_mean, vert_rot)
            self.out.log('acc vector mean vert rotated')
            self.out.log(mag_mean)
            mag_all_mean.loc[node_analysis, ['x', 'y', 'z']] = mag_mean.squeeze()

        self.out.log('df with all mean of magnetometers')
        self.out.log('(use only x and y axis)')
        self.out.log(mag_all_mean)
        self.out.log('overall mean vector to align all k-ceptors')
        self.out.log('(use only x and y axis)')
        self.out.log(mag_all_mean.mean().to_numpy())

        self.out.log('NOW WE FIND THE ROTATION TO APPLY TO EACH K-CEPTOR')
        # generate reference vector where all k-ceptor should point at
        ref_nodes = ['dorsal', 'base']
        references = mag_all_mean.loc[ref_nodes]
        self.out.log("mag references: {}\n".format(ref_nodes),references.to_numpy())
        mag_vector = references.to_numpy().mean(axis=0)
        ref_mag = mathutils.Vector((mag_vector[0],mag_vector[1],0))  # using mean of all k-ceptors
        
        self.out.log('reference mean vector', ref_mag)
        for node_analysis in self.node_list:
            # get mean of k-ceptor from df
            mag_mean = mag_all_mean.loc[node_analysis].to_numpy()
            # generate mag vector using X and Y only
            vec2 = mathutils.Vector((mag_mean[0], mag_mean[1], 0))
            self.out.log('mean vetor of k-ceptor', vec2)
            # get rotation
            func_rot = vec2.rotation_difference(ref_mag)
            # adding a correction to rotate -90deg in z axis
            # this correction is needed... i dont know why :/
            # it might be the axis of mag data (X and Y)
            # and how the mathtuils vectos are defined
            # i get lost in this part of the rotations...
            correc_rot = mathutils.Quaternion((0.0, 0.0, 1.0), -np.pi)
            if ref_mag.y < 0:
                func_rot = func_rot @ correc_rot
            self.out.log('functional rotation to apply', func_rot)
            rot_vec = mathutils.Vector(mag_mean)
            rot_vec.rotate(mathutils.Quaternion(func_rot))
    
            self.results["func_rot"][node_analysis] = func_rot

    ### ============ heading calibration ============

    def heading_calib(self):
        assert "vert_rot" in self.results.keys(), "The vert_calib should be ran before the heading_calib"
        assert "func_rot" in self.results.keys(), "The func_calib should be ran before the heading_calib"
        
        self.results["gamma"] = {}
        self.results["heading_rot"] = {}
        
        for node_name in self.node_list:
            self.out.log('Heading calculations, node: {}'.format(node_name), title=True)

            df = self.filtered_node_dfs[node_name]
            node_a = df.iloc[self.section_indexes['vert_i']:self.section_indexes['vert_s']]
            mat_quat = np.array([node_a.q_w, node_a.q_x, node_a.q_y, node_a.q_z])
            mat_quat = mat_quat.T
            
            try:
                vert_rot = self.results["vert_rot"][node_name]
                func_rot = self.results["func_rot"][node_name]
            except KeyError as e:
                self.out.log("FUNC_ROT_KEYS",self.results["func_rot"].keys())
                raise e

            # apply previous rotations to quats
            for i in range(len(mat_quat)):
                quat_i = _Quaternion((mat_quat[i, :]))
                new_quat = quat_i @ vert_rot.inverted() @ func_rot.inverted()
                mat_quat[i, :] = [new_quat.w, new_quat.x, new_quat.y, new_quat.z]
        
            # obtain mean
            quat_mean = np.mean(mat_quat, axis=0)
            self.out.log('{} quat vector mean: {}'.format(node_name, _Quaternion(quat_mean)))
            quat_mean = _Quaternion((quat_mean))
        
            # generate reference lateral vector
            lateral = _Vector((0, 1, 0))  # Y axis in node coordinates
            self.out.log(f'{node_name} Lateral vector in local coords: {lateral}')

            # rotate LATERAL with quat_mean
            lateral.rotate(quat_mean)
            self.out.log(f'{node_name} Lateral vector in world coords: {lateral}')
            
            # get GAMMA angle to make correction
            gamma = np.arctan2(lateral.y, lateral.x)
            self.results["gamma"][node_name] = gamma
            self.out.log(f'{node_name} gamma [deg] to rotate the k-ceptor: {-gamma *  180 / np.pi}')
            
            quat_rot = _Quaternion((0,0,1), - gamma)

            self.out.log(f'{node_name} heading_quat to align to world: {quat_rot}')
            
            self.results["heading_rot"][node_name] = quat_rot
 

class Calibrator(Base_Calibrator):
    def __init__(self, input_df, g_s, a_s, m_s, section_indexes, fc = defaults.fc, 
                        log = False, adjust_nodes = {}):
        super().__init__(input_df, g_s, a_s, m_s, section_indexes, fc, log, adjust_nodes)

    def run(self):
        self.out.log('RUNNING VERTICAL CALIBRATION', banner=True)
        self.vert_calib()
        self.out.log('RUNNING FUNCTIONAL CALIBRATION', banner=True)
        self.mag_func_calib()
        self.out.log('RUNNING HEADING CALIBRATION', banner=True)
        self.heading_calib()

        self.results["post_rot"] = {}
        for node_name in self.node_list:
            vert_rot = self.results["vert_rot"][node_name]
            func_rot = self.results["func_rot"][node_name]

            quat_jcs = self.JCS[node_name]
            post_quat = vert_rot.inverted() @ func_rot.inverted() @ quat_jcs
            self.results["post_rot"][node_name] = post_quat
            
    ### ============ functional calibration ============
    def func_calib(self):
        self._PCA()

        all_nodes = self.body_nodes
        
        self.results["gyro_int"] = {}
        self.results["func_rot"] = {}

        # now the integral must be calculated in all nodes
        self.out.log('Calculate Gyro Integral', title=True)
        for node_name in all_nodes:
            gyro_int = self._calculate_gyro_integral(node_name)
            self.results["gyro_int"][node_name] = gyro_int 
        
        self.out.log('Invert functional rotation', title=True)
        self._intermediate_results['inverted'] = {}
        for node_name in all_nodes:
            func_rot = self._invert_func_rot(node_name)
            if func_rot:
                self.results["func_rot"][node_name] = func_rot
            else:
                self.results["func_rot"][node_name] = self.results["PCA"][node_name]
        
        return self.results["func_rot"]
     ### ============ functional calibration ============
    def _PCA(self):
        assert "vert_rot" in self.results.keys(), "The vert_calib should be ran before the PCA analysis"
        self.results["PCA"] = {}
        self.results["angle_PCA"] = {}
        self._intermediate_results['PCA'] = {}
        for node_name in self.node_list:
            self.out.log('PCA, node:', node_name, title=True)
            node_df = self.filtered_node_dfs[node_name]
            vert_rot = self.results["vert_rot"][node_name]
            #  reading gyro data from functional phase
            func_i, func_s = self._get_func_range_ad(node_name)
            node_a = node_df.iloc[func_i:func_s]
            mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z] ) * self.g_s
            mat_gyro = mat_gyro.T
            # apply vert_rot rotation
            mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)

            # arrange data to feed PCA
            datos = np.array([mat_gyro[:, 0] , mat_gyro[:, 1]])
            datos = datos.T

            # PCA step
            eigen_vectors, sing_values, angle_new = self._PCA_np(datos)

            self.out.log('PCA singular values:', sing_values)
            self.out.log(f'PCA components from {node_name}: [{eigen_vectors[:,0]} {eigen_vectors[:,1]}]')
            # here comes the angle to rotate the z axis
            angle = angle_new

            self.results["angle_PCA"][node_name] = angle
            angle_deg = angle * 360 / (2 * np.pi)
            self.out.log('1st component angle:', angle_deg)
            self.out.log('angle to rotate:', (np.pi / 2 - angle) * 360 / (2 * np.pi))

            # generate rotation
            self.results["PCA"][node_name] = _Quaternion((0, 0, 1), np.pi/2- angle)
            self.out.log('PCA rotation:', self.results["PCA"][node_name])
            
            self._intermediate_results['PCA'][node_name] = (datos, eigen_vectors, sing_values, angle_deg)

    def _PCA_np(self, X):
        # center the data
        X_s = (X - X.mean(axis=0)) #/ (X.std(axis=0))
        # Compute SVD (its better than  the EINGENVECTOR method
        #  (no covariance problem))
        u, s, vh = np.linalg.svd(X_s.T, full_matrices=True)
        # enforce signs convention
        # (largest element in each eigenvector must be positive)
        max_index = np.argmax(np.abs(u), axis=0)
        for col in range(np.size(u, 1)):
            sign = np.sign(u[max_index[col], col])
            u[:, col] = u[:, col] * sign

        # Calculating the 1st component angle (w.r.t. WORLD X-Y)
        frst_comp_ang = np.arctan2(u[1, 0], u[0, 0])
        return u, s, frst_comp_ang

    def _calculate_gyro_integral(self, node_name):
        assert "vert_rot" in self.results.keys(), "The vert_calib should be ran before running this function"
        assert "PCA" in self.results.keys(), "The PCA should be ran before running this function"
        
        func_gyro_i, func_s = self._get_func_range_ad(node_name)
        func_gyro_s = (func_s - func_gyro_i) // 2 + func_gyro_i # half of the func_samples
        # reading gyro data from functional gyro-validation phase
        df = self.filtered_node_dfs[node_name]
        
        node_a = df.iloc[func_gyro_i:func_gyro_s]
        
        mat_gyro = np.array([node_a.g_x, node_a.g_y, node_a.g_z]) * self.g_s
        mat_gyro = mat_gyro.T
        
        # apply vert rotation
        vert_rot = self.results["vert_rot"][node_name]
        mat_gyro = apply_rot_2mat(mat_gyro, vert_rot)
        # apply func rotation
        func_rot = self.results["PCA"][node_name]
        mat_gyro = apply_rot_2mat(mat_gyro, func_rot)

        gyro_int = np.sum(mat_gyro, axis=0) * self.dt / 1000

        self.out.log('{:_<15s} (calc integral) gyro_int: {} (rads) | {} (deg)'.format(node_name, _Vector(gyro_int*np.pi/180), _Vector(gyro_int) ))

        plot_values = (mat_gyro,)
        try:
            self._intermediate_results['GYRO_INT'][node_name] = plot_values
        except KeyError:
            self._intermediate_results['GYRO_INT'] = { node_name: plot_values }

        return gyro_int
        
    def _invert_func_rot(self, node_name, bones_change_180 = defaults.trunk_nodes):
        gyro_int = self.results["gyro_int"][node_name]
        #Check if gyro int should be inverted
        if node_name in bones_change_180:
            if gyro_int[1] < 0: invert_gyro_int = True
            else:               invert_gyro_int = False
        else:
            if gyro_int[1] > 0: invert_gyro_int = True
            else:               invert_gyro_int = False
       
        # perform the inversion
        angle = self.results["angle_PCA"][node_name]
        if invert_gyro_int:
            func_rot = _Quaternion((0, 0, 1), np.pi / 2 - angle + np.pi)
            gyro_int = gyro_int * -1
            self.out.log('{:_<15s} gyro changed 180 deg: {}'.format(node_name, _Vector(gyro_int)))
            self.results["gyro_int"][node_name] = gyro_int
            self._intermediate_results['inverted'][node_name] = True
        else:
            func_rot = None
            self.out.log('{:_<15s} gyro NOT changed 180 deg'.format(node_name))
            self._intermediate_results['inverted'][node_name] = False

        return func_rot
