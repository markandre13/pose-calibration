# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's pose(segment) calibration
#
# http://chordata.cc
# contact@chordata.cc
#

# Copyright 2022-2023 Marcos Uriel Maillot, Bruno Laurencich
#
# This file is part of Chordata Motion's pose(segment) calibration.
#
# Chordata Motion's pose(segment) calibration is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's pose(segment) calibration.  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from warnings import warn

version_tuple = (0,2,0)
version_post = ""
__version__ = "{}.{}.{}{}".format(*version_tuple, "-"+version_post if version_post else "" )

#
# Sensor resolutions
# these values are used to convert the raw 16bit output from the sensors
# to real units
#

a_s = 0.000244
m_s = 0.00014
g_s = 0.07


#
# cut frequency for filter 
#

fc = 3


#
# Empiric correction for soft-tissue artifact
# this was found empirically
#

EMP_STA_FACTOR = 0.5

# trunk_nodes = ('neck', 'dorsal', 'base')
trunk_nodes = ('neck', 'dorsal', 'base','l-clavicle','r-clavicle')
# added clavicles nodes
# arms_nodes = ('l-clavicle', 'l-upperarm', 'l-lowerarm', 'l-hand','r-clavicle','r-upperarm', 'r-lowerarm', 'r-hand')
arms_nodes = ('l-upperarm', 'l-lowerarm', 'l-hand','r-upperarm', 'r-lowerarm', 'r-hand')

left_leg_nodes = ('l-upperleg', 'l-lowerleg', 'l-foot')
right_leg_nodes = ('r-upperleg', 'r-lowerleg', 'r-foot')
legs_nodes = left_leg_nodes + right_leg_nodes
body_nodes = trunk_nodes + arms_nodes + legs_nodes

class JCS_Nodes:
    trunk_nodes = trunk_nodes
    arms_nodes = arms_nodes
    left_leg_nodes = left_leg_nodes
    right_leg_nodes = right_leg_nodes
    legs_nodes = legs_nodes
    body_nodes = body_nodes
    
    def _filter_node_list(self, orig_list, node_list, verbose=False ):
        filt_list = []
        for node in orig_list:
            if node in node_list:
                filt_list.append(node)
            elif verbose:
                warn(f'Node `{node}` not found in DF, removing it from calibration process', stacklevel=2)
                self.out.log(f'Node `{node}` not found in DF, removing it from calibration process', title=True)
        
        return tuple(filt_list)
    
    def check_nodes_in_df(self, node_list):
        self.body_nodes = self._filter_node_list(self.body_nodes, node_list, verbose=True)
        self.trunk_nodes = self._filter_node_list(self.trunk_nodes, node_list)
        self.arms_nodes = self._filter_node_list(self.arms_nodes, node_list)
        self.left_leg_nodes = self._filter_node_list(self.left_leg_nodes, node_list)
        self.right_leg_nodes = self._filter_node_list(self.right_leg_nodes, node_list)
        self.legs_nodes = self._filter_node_list(self.legs_nodes, node_list)
        
        